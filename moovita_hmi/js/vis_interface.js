/*******************************************************************************
 * Global Declaration
 ******************************************************************************/
var LOAD_STATUS = 0;

/*******************************************************************************
 * Function Prototypes
 ******************************************************************************/
function moovita_load (filename)
{
    if(MY_VISUALIZER.isWebSocketOpen)
    {
	    if(!filename)
		    filename = sessionStorage.getItem('OLIVE_PD_LOC');

	    LOAD_STATUS = 0;
	    WS.send(JSON.stringify({
		    type: "LOAD",
	      	attribute: filename
	    }));
    }
}

function moovita_pause ()
{
	if(LOAD_STATUS && MY_VISUALIZER.isWebSocketOpen)
	{
		WS.send(JSON.stringify({
			type: "PAUSE",
		  	attribute: 'none'
		}));
	}
}

function moovita_play (time)
{
	if(LOAD_STATUS && MY_VISUALIZER.isWebSocketOpen)
	{
		WS.send(JSON.stringify({
			type: "PLAY",
		  	attribute: time
		}));
	}
}

function moovita_playrate (speed)
{
	if(LOAD_STATUS && MY_VISUALIZER.isWebSocketOpen)
	{
		WS.send(JSON.stringify({
			type: "PLAYRATE",
		  	attribute: speed
		}));
	}
}

function moovita_seek (val)
{
	if(LOAD_STATUS && MY_VISUALIZER.isWebSocketOpen)
	{
		WS.send(JSON.stringify({
			type: "SEEK",
		  	attribute: val
		}));
	}
}

function moovita_step (val)
{
	if(LOAD_STATUS && MY_VISUALIZER.isWebSocketOpen)
	{
		WS.send(JSON.stringify({
			type: "STEP",
		  	attribute: val
		}));
	}
}

function moovita_stop ()
{
	if(LOAD_STATUS && MY_VISUALIZER.isWebSocketOpen)
	{
		WS.send(JSON.stringify({
			type: "STOP",
		  	attribute: 'none'
		}));
	}
}
