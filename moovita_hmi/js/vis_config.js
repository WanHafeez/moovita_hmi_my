// mvtVisualizer config parameters
MVT_VISUALIZER = {
	config: {
		cameraFreeHeight: 60,
		cameraLockHeight: 90,
		cameraTrackHeight: 10,
		cameraTrackFar: 40,
		carDetected: "camaro",	
		carEgo: "veyron",
        colorGo: [0.2, 0.8, 0.1],
        colorSlow: [0.7, 0.7, 0.1],
        colorStop: [0.8, 0.2, 0.1],
        curbColor: '#00e5ff',
		defaultAvOrient: 0,
		globalOffsetX: 0,
		globalOffsetY: 0,
        groundTextureUrl: 'curbLines/futurise_tnb/tnb.png',
		groundTotalTransX: 79.328,
		groundTotalTransY: 210.79,
		groundTotalWidth: 831.409,
		groundTotalDepth: 707.438,
		groundTotalRotateZ: -90,
        isXYAxisRevert: true,
		numPedRecv: 50,
		numStaticObjRecv: 1000,
        numDynamicObjRecv: 1000,
        numUnknownObjRecv: 500,
		numVehRecv: 50,
        parkDetectColor: '#ffcd33',
        parkConfirmColor: '#33ff62',
		scaleSto: 1,
		scalePed: 3,
        speedStop: 0.1,
        speedSlow: 5.0,
        stoStop: 1.0,
		velMultipler: 400,
		webSocketIP: window.location.hostname + ":9052/ws",
        wireFrameVehicleColor: 0x0099ff,
        wireFrameObstacleColor: 0xff0089,
		zoneColor: 0x4dd678,
		zoneWidth: 3.2,
		zoneStep: 1
	},
	cars: {
		"alphard": {
			name:	"Toyota Alphard",
			url: 	"models/bin/toyotaAlphard/Toyota_Alphard.js",
			scale: 0.015,
			init_material: 4,
			materials: null
		},
		"camaro": {
			name: 	"Chevrolet Camaro",
			url:	"models/bin/camaro/CamaroNoUv_bin.js",
			scale: 0.20,
			init_material: 0,
			materials: null
		},
		"gallardo": {
			name: 	"Lamborghini Gallardo",
			url:	"models/bin/gallardo/GallardoNoUv_bin.js",
			scale: 0.0125,
			init_material: 13,
			materials: null
		},
		"rdm": {
			name: 	"Pod Zero",
			url:	"models/obj/pod_zero/pdz_render_model_Plain.obj",
			scale: 0.005,
			init_material: 13,
			materials: null
		},
		"veyron": {
			name:	"Bugatti Veyron",
			url: 	"models/bin/veyron/VeyronNoUv_bin.js",
			scale: 0.015,
			init_material: 8,
			materials: null
		}
	},
    layers: {
        curb: {
            color:  0x0099ff,
            url:    "curbLines/cetran-13.07.19/curbline.shp"
        },
        landmark:
        {
            color:  0xffffff,
            url:    "curbLines/cetran-13.07.19/lanemark.shp"
        },
        yellowline:
        {
            color:  0xffff00,
            url:    "curbLines/cetran-13.07.19/yellowline.shp"
        }
    },
	pedestrian: {url: "models/collada/pedestrian/avatar.dae"},
	skyBoxes: {
		galaxy: {
			urls: [ "textures/cube/MilkyWay/dark-s_px.jpg", "textures/cube/MilkyWay/dark-s_nx.jpg",
				"textures/cube/MilkyWay/dark-s_py.jpg", "textures/cube/MilkyWay/dark-s_ny.jpg",
				"textures/cube/MilkyWay/dark-s_pz.jpg", "textures/cube/MilkyWay/dark-s_nz.jpg" ]
		},
		skyGround: {
			urls: [ "textures/cube/SkyGround/right.png", "textures/cube/SkyGround/left.png",
				"textures/cube/SkyGround/top.png", "textures/cube/SkyGround/bottom.png", 
				"textures/cube/SkyGround/front.png", "textures/cube/SkyGround/back.png"]
		},
        moovita: {
            urls: [ "textures/cube/Hope_SkyBox/left.png", "textures/cube/Hope_SkyBox/right.png",
				"textures/cube/Hope_SkyBox/up.jpg", "textures/cube/Hope_SkyBox/bottom.jpg", 
				"textures/cube/Hope_SkyBox/back.png", "textures/cube/Hope_SkyBox/front.png"]
        }
	}
}
