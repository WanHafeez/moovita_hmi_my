/***********************************************************************
 * Global Declaration
 **********************************************************************/
var MY_HMI;         // Main human machine interface
var MY_VISUALIZER;	// Main visualizer obj
var WS;			    // Websocket handler
var WSTIMEOUT;		// Websocket timeout reconnecter

/***********************************************************************
 * Webpage initialization
 **********************************************************************/
/// When load ready
$(document).ready(function() {
    /// Init webgl due to it lags other stuff first
    init_webgl();
});

/***********************************************************************
 * Human Machine Interface Struct Prototypes
 **********************************************************************/
var moovita_HMI = function() {
    this._curPage = 0;
    this._globStatus = 0;
    this._isDriverValid = 0;
    this._isFullScreen = 0;
    this._numCurrentList = 1;
    this._parkStatus = 0;
    this._remoteStatus = 0;
}

moovita_HMI.prototype = {
    driver_logout : function (self)
    {
        location.reload();
    },
    preparation_button_next : function (self)
    {
        MY_HMI._numCurrentList++;
        if (MY_HMI._numCurrentList > 3)
            MY_HMI._numCurrentList = 3;
        else
        {
            $('#hmi-info-list-' + (MY_HMI._numCurrentList - 1)).hide("slide", {
                direction: "left"
            }, 500);
            $('#hmi-info-list-' + MY_HMI._numCurrentList).show("slide", {
                direction: "right"
            }, 500, function(){
                setTimeout(function() {if (MY_HMI._numCurrentList == 3)$("#hmi-info-driver-name").focus();}, 500);
            });

            /// Set front-end text
            if ($("#hmi-info-nav-left").css("opacity") == "0")
                $("#hmi-info-nav-left").css("opacity", "1")

            if (MY_HMI._numCurrentList == 2)
                $('#hmi-info-list-1 .simplebar-track.vertical').css("display", "none");
            else if (MY_HMI._numCurrentList == 3)
            {
                $("#hmi-info-title").html("ENTER THE DRIVER'S NAME");
                $("#hmi-info-nav-right").css("opacity", "0");
                $(".hmi-wrapper .hmi-rdm-ready").fadeIn();
            }
            $("#hmi-info-nav-n").html(MY_HMI._numCurrentList);
            
            /// Set progress circle
            $('#hmi-rdm-base-progress').circleProgress('value', MY_HMI._numCurrentList / 3); 
        
        }
    },
    preparation_button_prev : function (self)
    {
        MY_HMI._numCurrentList--;
        if (MY_HMI._numCurrentList < 1)
            MY_HMI._numCurrentList = 1;
        else
        {
            $('#hmi-info-list-' + (MY_HMI._numCurrentList + 1)).hide("slide", {
                direction: "right"
            }, 500);
            $('#hmi-info-list-' + MY_HMI._numCurrentList).show("slide", {
                direction: "left"
            }, 500);
            
            if ($("#hmi-info-nav-right").css("opacity") == "0")
                $("#hmi-info-nav-right").css("opacity", "1")
          
             /// Set front-end text
            if (MY_HMI._numCurrentList == 1)
            {
                $("#hmi-info-nav-left").css("opacity", "0");
                $('#hmi-info-list-1 .simplebar-track.vertical').css("display", "block");
            }
            if (MY_HMI._numCurrentList < 3)
            {
                $("#hmi-info-title").html("SYSTEM/HARDWARE CHECK");
                $("#hmi-info-driver-name, #hmi-info-driver-wrapper").removeClass("warning");
                $(".hmi-wrapper .hmi-rdm-ready").fadeOut();
            }
            $("#hmi-info-nav-n").html(MY_HMI._numCurrentList);
            
            /// Set progress circle
            $('#hmi-rdm-base-progress').circleProgress('value', MY_HMI._numCurrentList / 3);            
        }
    },
    prepartion_button_start: function(self)
    {
        if (!self._isDriverValid)
            return;
        
        var _padding = parseFloat($(".hmi-wrapper .hmi-rdm-base-c1").css("padding").replace("px", ""));
        $(".hmi-wrapper .hmi-rdm-base-c1").css({width: ($(".hmi-wrapper .hmi-rdm-base-c1").width() + (2 * _padding)) + "px",
            padding: _padding + "px"});
        $(".hmi-info").hide();
        $(".hmi-rdm").animate({width: "100%"}, 500, function()
        {
            $(".hmi-rdm-topic").animate({opacity: 0}, 500, function(){
                $(".hmi-rdm-car").animate({top: "-250%"}, 500, "easeInCubic", function(){
                    $(".hmi-wrapper").fadeOut(function(){
                        $("#visualizer-wrapper").fadeIn();
                        self._curPage = 1;
                        $("#vs-driver-info span:first-of-type").html("Hi, " + $("#hmi-info-driver-name").val());
                    });
                });
            });
        });
    },
    set_driver_valid: function(self, isValid)
    {
        self._isDriverValid = isValid;
    },
	start_all: function (self)
	{
        if (self._globStatus == 0)
        {
            self._globStatus = 1;
            /// Send websocket message
            WS.send(JSON.stringify({
                type: "START",
                attribute: "ALL"
            }));
            
            self._globStatus = 2;
            btn_start_on('btn-start-all');
        }
    },
	stop_all: function (self)
	{
        if (self._globStatus == 2)
        {
            self._globStatus = 1; 
            /// Send websocket message
            WS.send(JSON.stringify({
                type: "STOP",
                attribute: "ALL"
            }));
            
            self._globStatus = 0;
            btn_start_off('btn-start-all');
        }
    },
	start_park: function (self)
	{
        if (self._parkStatus == 0)
        {
            self._parkStatus = 1;
            /// Send websocket message
            WS.send(JSON.stringify({
                type: "START",
                attribute: "PARK"
            }));
            
            self._parkStatus = 2;
            btn_start_on('btn-start-parking');
        }
    },
	stop_park: function (self)
	{
        if (self._parkStatus == 2)
        {
            self._parkStatus = 1; 
            /// Send websocket message
            WS.send(JSON.stringify({
                type: "STOP",
                attribute: "PARK"
            }));
            
            self._parkStatus = 0;
            btn_start_off('btn-start-parking');
        }
    },
    start_remote: function (self)
	{
        if (self._remoteStatus == 0)
        {   
            
            self._remoteStatus = 1; 
            /// Send websocket message
            WS.send(JSON.stringify({
                type: "START",
                attribute: "REMOTE"
            }));
        
            self._remoteStatus = 2;
            btn_start_on('btn-start-remote');
        }
    },
    stop_remote: function (self)
	{
        if (self._remoteStatus == 2)
        {
            self._remoteStatus = 1; 
            // / Send websocket message
            WS.send(JSON.stringify({
                type: "STOP",
                attribute: "REMOTE"
            }));
            
            self._remoteStatus = 0;
            btn_start_off('btn-start-remote');
        }
    },
    toggle: function (self, id, checked)
    {
        var _type = "STOP";
        if (!checked)
            _type = "START"
            
        /// Send websocket message
		WS.send(JSON.stringify({
			type: _type,
		  	attribute: id
		}));
    }

}


/***********************************************************************
 * Visualizer Struct Prototypes
 **********************************************************************/
var moovita_VIZ = function() 
{
	// Parameters declaration
	this.animateId;
	this.animPed = [];
	this.avIndex;
	this.avSpeed = 0.0;
	this.camera, this.cameraCube;
	this.cameraMode = 2; // 0: Free, 1: Top, 2: Track
	this.clock = new THREE.Clock();
	this.container = $("#visualizer-wrapper");
	this.controlPL;
    this.controlTB = new THREE.Vector3();
	this.curbMinX = 5000;
	this.curbMinY = 5000; 
	this.curbMaxX = 0;
	this.curbMaxY = 0;
	this.displayOptions = [true, true, true, true, true, true];
    this.gauge;
	this.glScene = new THREE.Scene();
	this.glSceneCube = new THREE.Scene();
	this.groundWidth = MVT_VISUALIZER.config.groundTotalWidth;
	this.groundDepth = MVT_VISUALIZER.config.groundTotalDepth;
    this.isMouseDown = false;
	this.isWebSocketOpen = false;
	this.loaderBin = new THREE.BinaryLoader();
	this.loaderCollada = new THREE.ColladaLoader();
	this.loaderJson = new THREE.JSONLoader();
	this.loaderObj = new THREE.OBJLoader();
    this.lineMeshNav;
    this.lineMeshNavPrevLength = 0;
    this.lineMeshNavMat = new THREE.LineBasicMaterial({color: 0xffffff, linewidth: 5, opacity: 1});
    this.minSpeedEstimate = 2.77778;
	this.mlib;
	this.mouse = new THREE.Vector2();
	this.moveForward = false;
	this.moveBackward = false;
	this.moveLeft = false;
	this.moveRight = false;
	this.objMesh = [];
	this.objMeshCube = [];
	this.objMeshMdf = [];
	this.objMeshNav = [];
    this.objMeshObjWf = [];
	this.objMeshPed = [];
	this.objMeshDyo = [];
	this.objMeshSto = [];
	this.objMeshVeh = [];
    this.objMeshVehWF = [];
    this.onMouseDownPosition = new THREE.Vector2();
    this.parkingLine;
    this.parkingLineDetMat = new THREE.LineBasicMaterial({color: MVT_VISUALIZER.config.parkDetectColor});
    this.parkingLineConMat = new THREE.LineBasicMaterial({color: MVT_VISUALIZER.config.parkConfirmColor});
    this.phi = 60;
    this.phiOnMouseDown = 60;
	this.prevTime = performance.now();
    this.radious = 50;
	this.renderer = new THREE.WebGLRenderer();
	this.skyboxUrl = MVT_VISUALIZER.skyBoxes.moovita.urls;
	this.textureCube;
    this.theta = -270;
    this.thetaOnMouseDown = -270;
	this.velocity = new THREE.Vector3();
	this.worldWidth = 256;
	this.worldHeight = 256;
	this.worldDepth = 256;
	this.zoneMaterial = new THREE.MeshLambertMaterial({color: MVT_VISUALIZER.config.zoneColor, opacity: 0.55, transparent: true});
	this.zoneOffset = new THREE.Vector3(0, 0.1, -MVT_VISUALIZER.config.zoneWidth / 2);
	this.zoneShape = new THREE.Shape([
		new THREE.Vector2( 0, 0 ),
		new THREE.Vector2( 0, MVT_VISUALIZER.config.zoneWidth ),
		new THREE.Vector2( 0.1, MVT_VISUALIZER.config.zoneWidth ),
		new THREE.Vector2( 0.1, 0 )
	]);
	
	// Build 3D glScene
	this.init_map(this);

	// Add cameras with control
	this.init_camera(this);

	// Add ego car
	this.init_mesh_car(this);

	// Add pedestrians
	this.init_mesh_pedestrian(this);

	// Add static obstacles
	this.init_mesh_obstacle(this);
    
    // Init gauge meter
    this.init_gauge(this);

	// Bind event handlers
	this.init_event_handler(this);

}

/*******************************************************************************
 * Class Prototype Definition
 ******************************************************************************/
moovita_VIZ.prototype = 
{
	gl_create_car: function (self, geometry, car, isEgo)
	{
        if (car != "rdm")
        {
		    geometry.sortFacesByMaterialIndex();

		    var m = new THREE.MultiMaterial(),
		        s = MVT_VISUALIZER.cars[ car ].scale;

		    for ( var i in MVT_VISUALIZER.cars[ car ].mmap ) {
			    m.materials[ i ] = MVT_VISUALIZER.cars[ car ].mmap[ i ];
		    }

		    var explodeModifier = new THREE.ExplodeModifier();
		    explodeModifier.modify(geometry);

		    for( var i = 0; i < MVT_VISUALIZER.config.numVehRecv; i++ )
		    {
			    var mesh = new THREE.Mesh( geometry, m );
			    mesh.rotation.y = MVT_VISUALIZER.config.defaultAvOrient; 
			    mesh.scale.set(s, s, s);
			    mesh.castShadow = true;

			    if( isEgo )
			    {
                    mesh.position.x = 0;
                    mesh.position.y = 0;
                    mesh.position.z = 0;
                    mesh.rotation.y = 0.0;
				    self.objMesh.push(mesh);
				    self.avIndex = self.objMesh.length - 1;
				    self.glScene.add(self.objMesh[self.avIndex]);
				    gl_clear_obj_memory(mesh);				
				    break;
			    }
			    else
			    {
                    mesh.position.x = mesh.position.y = mesh.position.z = -10;
				    mesh.visible = false;
				    self.objMeshVeh.push(mesh);
				    self.glScene.add(self.objMeshVeh[self.objMeshVeh.length - 1]);
				    gl_clear_obj_memory(mesh);
			    }
		    }

		    gl_clear_obj_memory(explodeModifier);
		    gl_clear_obj_memory(m);
        }
        else
        {

            var count = 0;
            geometry.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                   //child.material = material;
                    if (count >= 0 && count < 70) 
                        child.material = self.mlib[ "Orange" ];
                    else if (count == 70) 
                        child.material = self.mlib[ "Black" ];
                    else if (count == 71) 
                        child.material = self.mlib[ "Blue" ];
                    else if (count == 72) 
                        child.material = self.mlib[ "Gold" ];
                    else if (count == 73) 
                        child.material = self.mlib[ "Bronze" ];
                    else if (count == 74) 
                        child.material = self.mlib[ "Carmine" ];
                    else if (count == 75)
                        child.material = self.mlib[ "Black" ];
                    else if (count == 76)
                        child.material = self.mlib[ "White metal" ]; 
                    else if (count == 79)
                        child.material = self.mlib[ "Red" ]; 
                    else if (count == 78)
                        child.material = self.mlib[ "Red" ]; 
                    else if (count == 77)
                        child.material = self.mlib[ "Red" ]; 
                    count = count + 1;
                }
            } );

            geometry.position.x = 0;
            geometry.position.y = 0;
            geometry.position.z = 0;
            geometry.rotation.y = 0.0;
            geometry.scale.x = 0.01;
            geometry.scale.y = 0.01;
            geometry.scale.z = 0.01;

		    self.objMesh.push(geometry);
		    self.avIndex = self.objMesh.length - 1;
		    self.glScene.add(self.objMesh[self.avIndex]);
		    gl_clear_obj_memory(geometry);				
        }

	},
    gl_create_car_wire_frame: function (self) 
    {
        // Create vehicle wireframe
        var wfGeometry = new THREE.CubeGeometry(1, 1, 1);
        var wfMaterial = new THREE.MeshBasicMaterial({
            color : MVT_VISUALIZER.config.wireFrameVehicleColor,
            wireframe : true
        });

		for( var i = 0; i < MVT_VISUALIZER.config.numVehRecv; i++ )
		{
			var mesh = new THREE.Mesh( wfGeometry, wfMaterial );
			mesh.position.x = mesh.position.y = mesh.position.z = -10;
			mesh.rotation.y = MVT_VISUALIZER.config.defaultAvOrient; 
			mesh.visible = false;
			self.objMeshVehWF.push(mesh);
			self.glScene.add(self.objMeshVehWF[self.objMeshVehWF.length - 1]);
			gl_clear_obj_memory(mesh);
        }

    	gl_clear_obj_memory(wfGeometry);
		gl_clear_obj_memory(wfMaterial);
    },
    gl_create_obj_wire_frame: function (self) 
    {
        // Create vehicle wireframe
        var wfGeometry = new THREE.CubeGeometry(1, 1, 1);
        var wfMaterial = new THREE.MeshBasicMaterial({
            color : MVT_VISUALIZER.config.wireFrameObstacleColor,
            wireframe : true
        });

		for( var i = 0; i < MVT_VISUALIZER.config.numUnknownObjRecv; i++ )
		{
			var mesh = new THREE.Mesh( wfGeometry, wfMaterial );
			mesh.position.x = mesh.position.y = mesh.position.z = -10;
			mesh.rotation.y = MVT_VISUALIZER.config.defaultAvOrient; 
			mesh.visible = false;
			self.objMeshObjWf.push(mesh);
			self.glScene.add(self.objMeshObjWf[self.objMeshObjWf.length - 1]);
			gl_clear_obj_memory(mesh);
        }

    	gl_clear_obj_memory(wfGeometry);
		gl_clear_obj_memory(wfMaterial);
    },
	gl_generate_sprite: function (val) 
    {

		var canvas = document.createElement( 'canvas' );
		canvas.width = 16;
		canvas.height = 16;

		var context = canvas.getContext( '2d' );
		var gradient = context.createRadialGradient( canvas.width / 2, canvas.height / 2, 0, canvas.width / 2, canvas.height / 2, canvas.width / 2 );
		gradient.addColorStop( 0, 'rgba(255,255,255,1)' );
		gradient.addColorStop( 0.2, 'rgba(0,255,255,1)' );
		gradient.addColorStop( 0.4, val );
		gradient.addColorStop( 1, 'rgba(0,0,0,1)' );

		context.fillStyle = gradient;
		context.fillRect( 0, 0, canvas.width, canvas.height );

		return canvas;
	},
	gl_render: function (self) 
	{
		var time = performance.now();
		var delta = ( time - self.prevTime ) / 1000;

		if(self.cameraMode == 0)
		{
			self.velocity.x -= self.velocity.x * 10.0 * delta;
			self.velocity.z -= self.velocity.z * 10.0 * delta;

			//velocity.y -= 9.8 * 100.0 * delta; // 100.0 = mass

			if ( self.moveForward ) self.velocity.z -= MVT_VISUALIZER.config.velMultipler * delta;
			if ( self.moveBackward ) self.velocity.z += MVT_VISUALIZER.config.velMultipler * delta;

			if ( self.moveLeft ) self.velocity.x -= MVT_VISUALIZER.config.velMultipler * delta;
			if ( self.moveRight ) self.velocity.x += MVT_VISUALIZER.config.velMultipler * delta;

			self.controlPL.getObject().translateX( self.velocity.x * delta );
			self.controlPL.getObject().translateZ( self.velocity.z * delta );
			self.controlPL.getObject().position.y = MVT_VISUALIZER.config.cameraFreeHeight;
		}	
		else
		{
			if(self.avIndex >= 0)
			{
				if(self.cameraMode == 1)
				{
					self.camera.position.x = self.objMesh[self.avIndex].position.x;
					self.camera.position.z = self.objMesh[self.avIndex].position.z;
					self.camera.position.y = MVT_VISUALIZER.config.cameraLockHeight;
					self.camera.rotation.order = 'YXZ';
					self.camera.rotation.x = -Math.PI / 2;
					self.camera.rotation.y = -Math.PI / 2;
					self.camera.rotation.z = 0;
				}
				else if(self.cameraMode == 2)
				{
					self.camera.rotation.y = self.objMesh[self.avIndex].rotation.y;
					self.camera.position.y = MVT_VISUALIZER.config.cameraTrackHeight;
					self.camera.position.x = self.objMesh[self.avIndex].position.x - (-MVT_VISUALIZER.config.cameraTrackFar * Math.sin(-self.camera.rotation.y));
					self.camera.position.z = self.objMesh[self.avIndex].position.z + (-MVT_VISUALIZER.config.cameraTrackFar * Math.cos(-self.camera.rotation.y));
				}
				else if(self.cameraMode == 3)
				{
					self.camera.position.x = self.controlTB.x + self.objMesh[self.avIndex].position.x;
					self.camera.position.y = self.controlTB.y;
					self.camera.position.z = self.controlTB.z + self.objMesh[self.avIndex].position.z;
				}
				self.camera.lookAt(self.objMesh[self.avIndex].position);
			}
		}

		this.prevTime = time;
		this.renderer.render(self.glSceneCube, self.camera);
		this.renderer.render(self.glScene, self.camera);
	},
	hide_dyo: function (dyoCount)
	{
		// Hide unused vehicle
		for( var i = dyoCount; i < MVT_VISUALIZER.config.numUnknownObjRecv; i++ )
		{
            if( !this.objMeshObjWf[i].visible)
                 break;
            else
            {
			    this.objMeshObjWf[i].visible = false;
			    this.objMeshObjWf[i].needUpdate = true;
            }
		}		
	},
	hide_dyo_p: function (dyoCount)
	{
		// Hide unused dynamic obstacle
		for( var i = dyoCount; i < MVT_VISUALIZER.config.numDynamicObjRecv; i++ )
		{
			if( !this.objMeshDyo[i].visible )
				break;
			else
			{
				this.objMeshDyo[i].visible = false;
				this.objMeshDyo[i].needUpdate = true;
			}
		}		
	},
	hide_ped: function (pedCount)
	{
		// Hide unused pedestrian
		for( var i = pedCount; i < MVT_VISUALIZER.config.numPedRecv; i++ )
		{
			if( !this.objMeshPed[i].visible )
				break;
			else
			{
				this.objMeshPed[i].visible = false;
				this.objMeshPed[i].needUpdate = true;
			}
		}		
	},
	hide_sto: function (stoCount)
	{
		// Hide unused static obstacle
		for( var i = stoCount; i < MVT_VISUALIZER.config.numStaticObjRecv; i++ )
		{
			if( !this.objMeshSto[i].visible )
				break;
			else
			{
				this.objMeshSto[i].visible = false;
				this.objMeshSto[i].needUpdate = true;
			}
		}		
	},
	hide_veh: function (vehCount)
	{
		// Hide unused vehicle
		for( var i = vehCount; i < MVT_VISUALIZER.config.numVehRecv; i++ )
		{
            if( !this.objMeshVeh[i].visible && !this.objMeshVehWF[i].visible)
                 break;
            else
            {
			    if( this.objMeshVeh[i].visible )
			    {
				    this.objMeshVeh[i].visible = false;
				    this.objMeshVeh[i].needUpdate = true;
			    }

			    if( this.objMeshVehWF[i].visible )
			    {
				    this.objMeshVehWF[i].visible = false;
				    this.objMeshVehWF[i].needUpdate = true;
			    }
            }
		}		
	},
	init_camera: function (self)
	{
		// Add Cameras
		var width = window.innerWidth;
		var height = window.innerHeight;
		self.camera = new THREE.PerspectiveCamera( 50, width / height, 1, self.worldWidth * 10 );
		self.cameraCube = new THREE.PerspectiveCamera( 50, width / height, 1, self.worldWidth * 10 );

        // Add Tracker Ball Controls
		self.controlTB.x = self.radious * Math.sin( self.theta * Math.PI / 360 ) * Math.cos( self.phi * Math.PI / 360 );
		self.controlTB.y = self.radious * Math.sin( self.phi * Math.PI / 360 );
		self.controlTB.z = self.radious * Math.cos( self.theta * Math.PI / 360 ) * Math.cos( self.phi * Math.PI / 360 );

		// Add Pointer Lock Controls
		self.controlPL = new THREE.PointerLockControls( self.camera );
		self.controlPL.enabled = false;
		self.glScene.add( self.controlPL.getObject() );
		self.glSceneCube.add( self.controlPL.getObject() );

		// Remove mouse event prevent duplicate
		document.removeEventListener('mousemove', function(){});

		// Bind mouse event
		$("#overlay-vs, #panel-top-middle, #panel-bottom-middle").bind(
		{
			"mousedown touchstart": function(e) 
			{  
				if(self.cameraMode == 0)
			    	self.controlPL.enabled = true;
                else if(self.cameraMode == 3)
                {
                    self.thetaOnMouseDown = self.theta;
                    self.phiOnMouseDown = self.phi;
                    if ((e.clientX) && (e.clientY)) {
                        self.onMouseDownPosition.x = e.clientX;
                        self.onMouseDownPosition.y = e.clientY;
                    } else if (e.originalEvent) {
                        self.onMouseDownPosition.x = e.originalEvent.changedTouches[0].clientX;
                        self.onMouseDownPosition.y = e.originalEvent.changedTouches[0].clientY;
                        e.preventDefault();
                    }
                }
                self.isMouseDown = true;
			}, 
			"mouseup touchend": function(e) 
			{
				self.controlPL.enabled = false;
                self.isMouseDown = false;
                if ((e.clientX) && (e.clientY)) {
                    self.onMouseDownPosition.x = e.clientX - self.onMouseDownPosition.x;
                    self.onMouseDownPosition.y = e.clientY - self.onMouseDownPosition.y;
                } else if (e.originalEvent) { 
                    self.onMouseDownPosition.x = e.originalEvent.changedTouches[0].clientX - self.onMouseDownPosition.x;
                    self.onMouseDownPosition.y = e.originalEvent.changedTouches[0].clientY - self.onMouseDownPosition.y;                   
                }
			},
			"mousemove touchmove": function(e) 
			{
				if(self.cameraMode == 3 && self.isMouseDown)
				{
                    if ((e.clientX) && (e.clientY)) {
                        self.theta = - ( ( e.clientX - self.onMouseDownPosition.x ) * 0.5 ) + self.thetaOnMouseDown;
                        self.phi = ( ( e.clientY - self.onMouseDownPosition.y ) * 0.5 ) + self.phiOnMouseDown;
                    } else if (e.originalEvent) {
                        self.theta = - ( ( e.originalEvent.changedTouches[0].clientX - self.onMouseDownPosition.x ) * 0.5 ) + self.thetaOnMouseDown;
                        self.phi = ( ( e.originalEvent.changedTouches[0].clientY - self.onMouseDownPosition.y ) * 0.5 ) + self.phiOnMouseDown;                       
                    }
                    e.preventDefault();
					self.phi = Math.min( 180, Math.max( 0, self.phi ) );
                    
                    self.controlTB.x = self.radious * Math.sin( self.theta * Math.PI / 360 ) * Math.cos( self.phi * Math.PI / 360 );
                    self.controlTB.y = self.radious * Math.sin( self.phi * Math.PI / 360 );
                    self.controlTB.z = self.radious * Math.cos( self.theta * Math.PI / 360 ) * Math.cos( self.phi * Math.PI / 360 );
				}
			}	
		});

		// Bind zoom in/out camera features
		$("#overlay-vs, #panel-top-middle, #panel-bottom-middle").mousewheel(function(event, delta) 
		{
			var newfov = self.camera.fov - (event.deltaY * event.deltaFactor * 0.05);
			if ( newfov < 5 )
				self.camera.fov = 5;
			else if ( newfov > 100 )
				self.camera.fov = 100;
			else
				self.camera.fov = newfov;

			self.camera.updateProjectionMatrix();
			event.preventDefault();
		});

		// Bind keyboard events
		document.addEventListener( 'keydown', function(event){
			switch ( event.keyCode ) {
				case 38: // up
				case 87: // w
					self.moveForward = true;
					break;

				case 37: // left
				case 65: // a
					self.moveLeft = true; 
					break;

				case 40: // down
				case 83: // s
					self.moveBackward = true;
					break;

				case 39: // right
				case 68: // d
					self.moveRight = true;
					break;

				case 32: // space
				//if ( canJump === true ) velocity.y += 350;
				//canJump = false;
					break;

				case 67:
					self.camera.fov = 50;
					self.camera.updateProjectionMatrix();
					break;
			}
		});

		document.addEventListener( 'keyup', function(event){
			switch ( event.keyCode ) {
				case 38: // up
				case 87: // w
					self.moveForward = false;
					break;

				case 37: // left
				case 65: // a
					self.moveLeft = false; 
					break;

				case 40: // down
				case 83: // s
					self.moveBackward = false;
					break;

				case 39: // right
				case 68: // d
					self.moveRight = false;
					break;

				case 32: // space
				//if ( canJump === true ) velocity.y += 350;
				//canJump = false;
					break;
			}
		});

	},
	init_curb: function (self)
	{
		// Add lines
		var groundLineMaterial = new THREE.LineBasicMaterial({color: MVT_VISUALIZER.config.curbColor});
		var preShapeId = -1;
		var tmpX, tmpY;
		
		$.each( outline, function( key, value ) 
		{
			for(var i = 0; i < value.length; i++)
			{
				if(preShapeId != key)
				{
					preShapeId = key;
					var geometry = new THREE.Geometry();

					if(preShapeId >= 0)
					{
						var line = new THREE.Line(geometry, groundLineMaterial);
						self.glScene.add( line );
					}
				}

				tmpX = (parseFloat(value[i][1]) - MVT_VISUALIZER.config.globalOffsetX);
				tmpY = (parseFloat(value[i][0]) - MVT_VISUALIZER.config.globalOffsetY);
				
				if(tmpX < self.curbMinX) self.curbMinX = tmpX;
				if(tmpX > self.curbMaxX) self.curbMaxX = tmpX;
				if(tmpY < self.curbMinY) self.curbMinY = tmpY;
				if(tmpY > self.curbMaxY) self.curbMaxY = tmpY;
				geometry.vertices.push(new THREE.Vector3(tmpX, -1.0, tmpY));
			}
		});
	},
	init_event_handler: function (self)
	{
		// Menu full screen 
		$("#panel-bottom-left li").click(function()
		{
            // Toggle menu effect
            if (!$(this).hasClass("on"))
            {
                $("#panel-bottom-left li").removeClass("on");
                $(this).addClass("on");
                
                // Hide all and show specific panel
                $(".view-info-wrapper").hide();
                if ($(this).index() == 0) {$("#view-debug.view-info-wrapper").show();}
                else if ($(this).index() == 1) {$("#view-info.view-info-wrapper").show();}                
                else if ($(this).index() == 2) {$("#view-module.view-info-wrapper").show();}
                else if ($(this).index() == 3) {$("#view-notify.view-info-wrapper").show(); $("#view-notify-no-read").css("display", "none");}
                else if ($(this).index() == 4) {$("#view-help.view-info-wrapper").show();}
            }
            else
            {
                // Hide all and dont't show anything
                $("#panel-bottom-left li").removeClass("on");
                $(".view-info-wrapper").hide();
            }
		});

		// Menu camera
		$("#camera-view li").click(function()
		{
			if(self.cameraMode != $(this).index())
			{
				// Reinit camera and control to avoid awkward rotation
				self.glScene.remove( self.controlPL.getObject() );
				self.camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 1, self.worldWidth * 10 );

				// Set new camera index
				self.cameraMode = $(this).index();

				// Add control to new camera
				self.controlPL = new THREE.PointerLockControls(self.camera);
				self.controlPL.enabled = false;
				self.glScene.add( self.controlPL.getObject() );

				// Update new camera position if free mode
				if( self.cameraMode == 0 )
				{
					self.controlPL.getObject().translateX(self.objMesh[self.avIndex].position.x);
					self.controlPL.getObject().translateZ(self.objMesh[self.avIndex].position.z);
				}	
			}
		});

		// Menu display
		$("#display-opt li .view-info-button").click(function()
		{
			var power;
			if( $(this).hasClass("on") )
			{
				$(this).removeClass("on");
				power = false;
			}
			else
			{
				$(this).addClass("on");
				power = true;
			}

			self.displayOptions[$(this).parent("li").index()] = power;
            if( $(this).parent("li").attr('id') == "show-vs" )
			{
				// Case digital clock
				if( power )
                    $("#container-vs").css("display", "block");
                else
                    $("#container-vs").css("display", "none");
			}
			else if( $(this).parent("li").attr('id') == "show-gpp" )
			{
				// Case global path
				for( var i = 0; i < self.objMeshMdf.length; i++ )
				{
					self.objMeshMdf[i].visible = power;
					self.objMeshMdf[i].needUpdate = true;
				}
			}
			else if( $(this).parent("li").attr('id') == "show-lppl" && typeof self.lineMeshNav.visible !== "undefined" )
			{
				// Case local path planning line
                self.lineMeshNav.visible = power;
                self.lineMeshNav.needUpdate = true;
			}
			else if( $(this).parent("li").attr('id') == "show-lppz" )
			{
				// Case local path planning zone
			    for( var i = 0; i < self.objMeshNav.length; i++ )
			    {
				    self.objMeshNav[i].visible = power;
				    self.objMeshNav[i].needUpdate = true;
			    }
            }
			else if( $(this).parent("li").attr('id') == "show-vbb" )
			{
		        // Display vehicle bounding box
		        for( var i = 0; i < MVT_VISUALIZER.config.numVehRecv; i++ )
		        {
                    if (self.objMeshVehWF[i].visible != power)
                    {
                        self.objMeshVehWF[i].visible = power;
                        self.objMeshVehWF[i].needUpdate = true;
                    } 
                    else
                        break;
                }
            }
			else if( $(this).parent("li").attr('id') == "show-vm" )
			{
		        // Display vehicle model
		        for( var i = 0; i < MVT_VISUALIZER.config.numVehRecv; i++ )
		        {
                    if (self.objMeshVeh[i].visible != power)
                    {
                        self.objMeshVeh[i].visible = power;
                        self.objMeshVeh[i].needUpdagte = true;
                    } 
                    else
                        break;
                }
            }
			else if( $(this).parent("li").index() == 6 )
			{
				// Case steering wheel and indicators
				if( power )
                    $("#container-vi").css("display", "block");
                else
                    $("#container-vi").css("display", "none");
            }
		});

        // Clear notification message
        $("#view-notify-clear").bind("click touch", function(){
            $("#view-notify-no-message").css("display", "block");
            $("#view-notify .simplebar-content li").remove();
        });

        // Request new path events
        $("#next-stop input").change(function ()
        {
            if (MY_VISUALIZER.avSpeed < 1)
            {
                var msg = $(this).attr('id').split("-");
                WS.send(JSON.stringify({
                    type: "REQ_PATH",
                    attribute: msg[1]
                }));
                $("#dest-name").html($("label[for='" + $(this).attr('id') + "']").html());
                $("#destination-info img").attr("src", $(this).attr('data-dest-src'));
            }
            else
            {
                $(this).attr("checked", false);
                notify_msg("Warning", "Mission Planner", "Please stop vehicle first before submit a new destinatioon");
            }
        });
        
        // Request new path from next stop button
        $("#next-stop-btn").bind("click touch", function(){
            var counts_ = $("#next-stop").find($("input") );
            var id_ = 0;
            $("#next-stop input:checked").each(function() {
                var msg = $(this).attr('id').split("-");
                id_ = (parseInt(msg[1]) + 1) % counts_.length;
            });
            
            $("#bs-" + id_).click();
        });
	},
    init_gauge: function (self)
    {
        // Enable following line for auto sizing
        //$('#vs-gauge').attr({"width": ($(document).width() * .5) + "px", "height": ($(document).width() * .25) + "px"});
        var opts = {
            angle: 0.25, // The span of the gauge arc
            lineWidth: 15 / $(document).width(), // The line thickness
            radiusScale: 1, // Relative radius
            limitMax: false,     // If false, max value increases automatically if value > maxValue
            limitMin: false,     // If true, the min value of the gauge will be fixed
            colorStart: '#476991',   // Colors
            colorStop: '#476991',    // just experiment with them
            strokeColor: '#080A0D',  // to see which ones work best for you
            shadowColor: "#343642",
            generateGradient: true,
            highDpiSupport: true,     // High resolution support
        };
        var target = document.getElementById('vs-gauge');
        self.gauge = new Donut(target).setOptions(opts); // create sexy gauge!
        self.gauge.maxValue = 300; // set max gauge value
        self.gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
        self.gauge.animationSpeed = 32; // set animation speed (32 is default value)
        self.gauge.set(0); // set actual value
    },
	init_map: function (self)
	{
		// Add Skybox
		self.initSkyBox(self);

        // Add shp layers
        /*self.init_shape_file(self, MVT_VISUALIZER.layers.curb.url, MVT_VISUALIZER.layers.curb.color, 0.25);
        self.init_shape_file(self, MVT_VISUALIZER.layers.landmark.url, MVT_VISUALIZER.layers.landmark.color, 0.1);
        self.init_shape_file(self, MVT_VISUALIZER.layers.yellowline.url, MVT_VISUALIZER.layers.yellowline.color, 0.1);
        */

		// Load ground texture with loader
        
		var mapLoader = new THREE.TextureLoader();

		// Load a resource
		mapLoader.load(
			// resource URL
			MVT_VISUALIZER.config.groundTextureUrl,
			// Function when resource is loaded
			function ( texture ) 
			{
				// Do something with the texture
				var groundMaterial = new THREE.MeshBasicMaterial( {map: texture, transparent: true, opacity: 0.95, color: 0xFFFFFF } );
				var groundgeometry = new THREE.PlaneGeometry(self.groundWidth, self.groundDepth);
				self.groundPlane = new THREE.Mesh( groundgeometry, groundMaterial );
				self.groundPlane.rotation.set(-Math.PI * 0.5, 0, 0, 'YXZ');
				self.groundPlane.position.y = -1;
				self.groundPlane.position.x = -self.curbMaxX;
				self.groundPlane.position.z = -self.curbMaxY;
				// GroundPlane.receiveShadow = true;
				self.glScene.add( self.groundPlane );

				// Update ground after add
                self.groundPlane.rotateZ (MVT_VISUALIZER.config.groundTotalRotateZ / 180 * Math.PI);
				self.groundPlane.translateY (MVT_VISUALIZER.config.groundTotalTransY);
				self.groundPlane.translateX (MVT_VISUALIZER.config.groundTotalTransX);
				self.groundPlane.needUpdate = true;
				$("#transY").val(0);
				$("#transX").val(0);
				$("#rotateZ").val(0);

		        // Add curb to glScene
		        self.init_curb(self);
			}
		);
        

	    // Renderer
	    self.init_renderer(self);

	    // Bind events
	    self.init_map_debugger(self);

        // Set grid helper
        //var gridHelper = new THREE.GridHelper( 500, 100, 0x00d8ff, 0x86b5ff);
        //self.glScene.add( gridHelper );   

	},
	init_map_debugger: function (self)
	{
		// Ground mesh modifier
		$("#updateGroundMesh").bind('click', function(){
			MVT_VISUALIZER.config.groundTotalTransX += parseFloat($("#transX").val());
			MVT_VISUALIZER.config.groundTotalTransY += parseFloat($("#transY").val());
			MVT_VISUALIZER.config.groundTotalRotateZ += parseFloat($("#rotateZ").val());
			self.groundPlane.translateY(parseFloat($("#transY").val()));
			self.groundPlane.translateX(parseFloat($("#transX").val()));
			self.groundPlane.rotateZ(parseFloat($("#rotateZ").val()) / 180 * Math.PI);
			self.groundPlane.needUpdate = true;
			console.log("x: "+ MVT_VISUALIZER.config.groundTotalTransX+ " y: " + MVT_VISUALIZER.config.groundTotalTransY + " rot:" + MVT_VISUALIZER.config.groundTotalRotateZ);
		});
	},
	init_material_lib: function(self)
	{
		self.mlib = {
			"Orange": 	new THREE.MeshLambertMaterial( { color: 0xff6600, envMap: self.textureCube, combine: THREE.MixOperation, reflectivity: 0.01 } ),
			"Blue": 	new THREE.MeshLambertMaterial( { color: 0x001133, envMap: self.textureCube, combine: THREE.MixOperation, reflectivity: 0.01 } ),
			"Red": 		new THREE.MeshLambertMaterial( { color: 0x660000, envMap: self.textureCube, combine: THREE.MixOperation, reflectivity: 0.01 } ),
			"Black": 	new THREE.MeshLambertMaterial( { color: 0x000000, envMap: self.textureCube, combine: THREE.MixOperation, reflectivity: 0.01 } ),
			"White":	new THREE.MeshLambertMaterial( { color: 0xffffff, envMap: self.textureCube, combine: THREE.MixOperation, reflectivity: 0.01 } ),

			"Carmine": 	new THREE.MeshPhongMaterial( { color: 0x770000, specular:0xffaaaa, envMap: self.textureCube, combine: THREE.MultiplyOperation } ),
			"Gold": 	new THREE.MeshPhongMaterial( { color: 0xaa9944, specular:0xbbaa99, shininess:50, envMap: self.textureCube, combine: THREE.MultiplyOperation } ),
			"Bronze":	new THREE.MeshPhongMaterial( { color: 0x150505, specular:0xee6600, shininess:10, envMap: self.textureCube, combine: THREE.MixOperation, reflectivity: 0.25 } ),
			"Chrome": 	new THREE.MeshPhongMaterial( { color: 0xffffff, specular:0xffffff, envMap: self.textureCube, combine: THREE.MultiplyOperation } ),

			"Orange metal": new THREE.MeshLambertMaterial( { color: 0xff6600, envMap: self.textureCube, combine: THREE.MultiplyOperation } ),
			"Blue metal": 	new THREE.MeshLambertMaterial( { color: 0x001133, envMap: self.textureCube, combine: THREE.MultiplyOperation } ),
			"Red metal": 	new THREE.MeshLambertMaterial( { color: 0x770000, envMap: self.textureCube, combine: THREE.MultiplyOperation } ),
			"Green metal": 	new THREE.MeshLambertMaterial( { color: 0x007711, envMap: self.textureCube, combine: THREE.MultiplyOperation } ),
			"Black metal":	new THREE.MeshLambertMaterial( { color: 0x222222, envMap: self.textureCube, combine: THREE.MultiplyOperation } ),
			"White metal": new THREE.MeshLambertMaterial( { color: 0xffffff, envMap: self.textureCube, combine: THREE.MultiplyOperation, reflectivity: 0.5 } ),

			"Pure chrome": 	new THREE.MeshLambertMaterial( { color: 0xffffff, envMap: self.textureCube } ),
			"Dark chrome":	new THREE.MeshLambertMaterial( { color: 0x444444, envMap: self.textureCube } ),
			"Darker chrome":new THREE.MeshLambertMaterial( { color: 0x222222, envMap: self.textureCube } ),

			"Black glass": 	new THREE.MeshLambertMaterial( { color: 0x101016, envMap: self.textureCube, opacity: 0.975, transparent: true } ),
			"Dark glass":	new THREE.MeshLambertMaterial( { color: 0x101046, envMap: self.textureCube, opacity: 0.25, transparent: true } ),
			"BlWhite":	new THREE.MeshLambertMaterial( { color: 0x668899, envMap: self.textureCube, opacity: 0.75, transparent: true } ),
			"Light glass":	new THREE.MeshBasicMaterial( { color: 0x223344, envMap: self.textureCube, opacity: 0.25, transparent: true, combine: THREE.MixOperation, reflectivity: 0.25 } ),

			"Red glass":	new THREE.MeshLambertMaterial( { color: 0xff0000, opacity: 0.75, transparent: true } ),
			"Yellow glass":	new THREE.MeshLambertMaterial( { color: 0xffffaa, opacity: 0.75, transparent: true } ),
			"Orange glass":	new THREE.MeshLambertMaterial( { color: 0x995500, opacity: 0.75, transparent: true } ),

			"Orange glass 50":	new THREE.MeshLambertMaterial( { color: 0xffbb00, opacity: 0.5, transparent: true } ),
			"Red glass 50": 	new THREE.MeshLambertMaterial( { color: 0xff0000, opacity: 0.5, transparent: true } ),

			"Fullblack rough":	new THREE.MeshLambertMaterial( { color: 0x000000 } ),
			"Black rough":		new THREE.MeshLambertMaterial( { color: 0x050505 } ),
			"Darkgray rough":	new THREE.MeshLambertMaterial( { color: 0x090909 } ),
			"Red rough":		new THREE.MeshLambertMaterial( { color: 0x330500 } ),

			"Darkgray shiny":	new THREE.MeshPhongMaterial( { color: 0x000000, specular: 0x050505 } ),
			"Gray shiny":		new THREE.MeshPhongMaterial( { color: 0x050505, shininess: 20 } )
		}

		// Toyota alphard materials
		MVT_VISUALIZER.cars[ "alphard" ].materials = 
		{
			body: [
				[ "Orange", 	self.mlib[ "Orange" ] ],
				[ "Blue", 	self.mlib[ "Blue" ] ],
				[ "Red", 	self.mlib[ "Red" ] ],
				[ "Green",	self.mlib[ "Green" ] ],
				[ "White", 	self.mlib[ "White" ] ],
				[ "Gold", 	self.mlib[ "Gold" ] ],
				[ "Bronze", 	self.mlib[ "Bronze" ] ],
				[ "Chrome", 	self.mlib[ "Chrome" ] ]
			],
		};

		m = MVT_VISUALIZER.cars[ "alphard" ].materials;
		mi = MVT_VISUALIZER.cars[ "alphard" ].init_material;

		MVT_VISUALIZER.cars[ "alphard" ].mmap = 
		{
			0: m.body[ mi ][ 1 ],		    // back / top / front torso
			1: self.mlib[ "Black" ],	    // tires
			2: self.mlib[ "Gray shiny" ], 	// window frame
			3: self.mlib[ "Bronze" ],	    // metalic frame
			4: self.mlib[ "Light glass" ],	// mirror
			5: self.mlib[ "Light glass" ],	// side mirror
			6: self.mlib[ "Black" ],	    // side window frame
			7: self.mlib[ "Gray shiny" ],	// frame + wipers
			8: self.mlib[ "Red" ],		    // backsignals
			9: self.mlib[ "Red" ],		    // backsignals
			10: self.mlib[ "Light glass" ],	// front light
			11: self.mlib[ "Light glass" ]	// front light
		};

		// Veyron materials
		MVT_VISUALIZER.cars[ "veyron" ].materials = 
		{
			body: [
				[ "Orange", 		self.mlib[ "Orange" ] ],
				[ "Blue metal", 	self.mlib[ "Blue metal" ] ],
				[ "Red metal", 		self.mlib[ "Red metal" ] ],
				[ "Green metal",	self.mlib[ "Green metal" ] ],
				[ "Black metal", 	self.mlib[ "Black metal" ] ],
				[ "Gold", 		    self.mlib[ "Gold" ] ],
				[ "Bronze", 		self.mlib[ "Bronze" ] ],
				[ "Chrome", 		self.mlib[ "Chrome" ] ],
                [ "Bronze", 		    self.mlib[ "Bronze" ] ]
			],
		};

		m = MVT_VISUALIZER.cars[ "veyron" ].materials;
		mi = MVT_VISUALIZER.cars[ "veyron" ].init_material;

		MVT_VISUALIZER.cars[ "veyron" ].mmap = 
		{
			0: self.mlib[ "Black rough" ],		// tires + inside
			1: self.mlib[ "Pure chrome" ],		// wheels + extras chrome
			2: m.body[ mi ][ 1 ], 			    // back / top / front torso
			3: self.mlib[ "Dark glass" ],		// glass
			4: m.body[ mi ][ 1 ],			    // sides torso
			5: self.mlib[ "Orange metal" ],		// engine
			6: self.mlib[ "Red glass 50" ],		// backlights
			7: self.mlib[ "Orange glass 50" ]	// backsignals
		};

		// Gallardo materials
		MVT_VISUALIZER.cars[ "gallardo" ].materials = {
			body: [
				[ "Orange", 	self.mlib[ "Orange" ] ],
				[ "Blue", 		self.mlib[ "Blue" ] ],
				[ "Red", 		self.mlib[ "Red" ] ],
				[ "Black", 		self.mlib[ "Black" ] ],
				[ "White", 		self.mlib[ "White" ] ],
				[ "Orange metal", 	self.mlib[ "Orange metal" ] ],
				[ "Blue metal", 	self.mlib[ "Blue metal" ] ],
				[ "Green metal", 	self.mlib[ "Green metal" ] ],
				[ "Black metal", 	self.mlib[ "Black metal" ] ],
				[ "Carmine", 	self.mlib[ "Carmine" ] ],
				[ "Gold", 		self.mlib[ "Gold" ] ],
				[ "Bronze", 	self.mlib[ "Bronze" ] ],
				[ "Chrome", 	self.mlib[ "Chrome" ] ],
                [ "Gray shiny", 	self.mlib[ "Gray shiny" ] ]
			]
		};
		m = MVT_VISUALIZER.cars[ "gallardo" ].materials;
		mi = MVT_VISUALIZER.cars[ "gallardo" ].init_material;

		MVT_VISUALIZER.cars[ "gallardo" ].mmap = {
			0: self.mlib[ "Pure chrome" ], 	// wheels chrome
			1: self.mlib[ "Black rough" ],  // tire
			2: self.mlib[ "Black glass" ], 	// windshield
			3: m.body[ mi ][ 1 ], 		    // body
			4: self.mlib[ "Red glass" ],    // back lights
			5: self.mlib[ "Yellow glass" ], // front lights
			6: self.mlib[ "Dark chrome" ]	// windshield rim
		};

		// Camero materials
		MVT_VISUALIZER.cars[ "camaro" ].materials = {
			body: [
				[ "Orange", 	self.mlib[ "Orange" ] ],
				[ "Blue", 		self.mlib[ "Blue" ] ],
				[ "Red", 		self.mlib[ "Red" ] ],
				[ "Black", 		self.mlib[ "Black" ] ],
				[ "White", 		self.mlib[ "White" ] ],
				[ "Orange metal", 	self.mlib[ "Orange metal" ] ],
				[ "Blue metal", 	self.mlib[ "Blue metal" ] ],
				[ "Red metal", 		self.mlib[ "Red metal" ] ],
				[ "Green metal", 	self.mlib[ "Green metal" ] ],
				[ "Black metal", 	self.mlib[ "Black metal" ] ],
				[ "Gold", 		self.mlib[ "Gold" ] ],
				[ "Bronze", 	self.mlib[ "Bronze" ] ],
				[ "Chrome", 	self.mlib[ "Chrome" ] ]
			]
		};
		m = MVT_VISUALIZER.cars[ "camaro" ].materials;
		mi = MVT_VISUALIZER.cars[ "camaro" ].init_material;

		MVT_VISUALIZER.cars[ "camaro" ].mmap = {
			0: m.body[ mi ][ 1 ], 			    // car body
			1: self.mlib[ "Pure chrome" ], 		// wheels chrome
			2: self.mlib[ "Pure chrome" ], 		// grille chrome
			3: self.mlib[ "Dark chrome" ], 		// door lines
			4: self.mlib[ "Light glass" ], 		// windshield
			5: self.mlib[ "Gray shiny" ],       // interior
			6: self.mlib[ "Black rough" ],      // tire
			7: self.mlib[ "Fullblack rough" ],  // tireling
			8: self.mlib[ "Fullblack rough" ]   // behind grille
		};
	},
	init_mesh_car: function (self)
	{
        // Initialize ego vehicle
		if( MVT_VISUALIZER.config.carEgo == "alphard" )
			var loader = self.loaderJson;
        if( MVT_VISUALIZER.config.carEgo == "rdm" )
            var loader = self.loaderObj;
		else
			var loader = self.loaderBin;

		loader.load( MVT_VISUALIZER.cars[MVT_VISUALIZER.config.carEgo].url, function(geometry) 
		{
			self.gl_create_car(self, geometry, MVT_VISUALIZER.config.carEgo, true);
			gl_clear_obj_memory(geometry);
		});

        // Initialize detected vehicle
		if( MVT_VISUALIZER.config.carDetected == "alphard" )
			var loader = self.loaderJson;
		else
			var loader = self.loaderBin;

		loader.load( MVT_VISUALIZER.cars[MVT_VISUALIZER.config.carDetected].url, function(geometry) 
		{
			self.gl_create_car(self, geometry, MVT_VISUALIZER.config.carDetected, false);
            self.gl_create_car_wire_frame(self);
            self.gl_create_obj_wire_frame(self);
			gl_clear_obj_memory(geometry);
		});
	},
	init_mesh_obstacle: function (self)
	{
		var cTextureB = new THREE.CanvasTexture( self.gl_generate_sprite('rgba(0,0,64,1)') );
        var cTextureR = new THREE.CanvasTexture( self.gl_generate_sprite('rgba(64,0,0,1)') );
		var materialB = new THREE.SpriteMaterial( {
			map: cTextureB,
			blending: THREE.AdditiveBlending
		} );
		var materialR = new THREE.SpriteMaterial( {
			map: cTextureR,
			blending: THREE.AdditiveBlending
		} );

		var s = MVT_VISUALIZER.config.scaleSto;
		for ( var i = 0; i < MVT_VISUALIZER.config.numStaticObjRecv; i++ ) 
		{
			particle = new THREE.Sprite( materialB );
			particle.scale.set(s, s, s);
			particle.visible = false;
			particle.position.x = particle.position.y = particle.position.z = 0;
			self.objMeshSto.push(particle);
			self.glScene.add(self.objMeshSto[self.objMeshSto.length - 1]);
			gl_clear_obj_memory(particle);
		}

		for ( var i = 0; i < MVT_VISUALIZER.config.numDynamicObjRecv; i++ ) 
		{
			particle = new THREE.Sprite( materialR );
			particle.scale.set(s, s, s);
			particle.visible = false;
			particle.position.x = particle.position.y = particle.position.z = 0;
			self.objMeshDyo.push(particle);
			self.glScene.add(self.objMeshDyo[self.objMeshDyo.length - 1]);
			gl_clear_obj_memory(particle);
		}

		gl_clear_obj_memory(materialB);
		gl_clear_obj_memory(cTextureB);
		gl_clear_obj_memory(materialR);
		gl_clear_obj_memory(cTextureR);
	},
	init_mesh_nav: function(numToAppend)
	{
		var zonegeometry;
		var v1 = new THREE.Vector3(0, 0.1, 0);
		var v2 = new THREE.Vector3(0, 0.1, 0);
		var extrudePath = new THREE.LineCurve3( v1, v2 );
		var extrudeSettings = {bevelEnabled: false, steps: 1, extrudePath: extrudePath};

		// Add Local path planning zone
		for(var i = 0; i < numToAppend; i++)
		{
			zonegeometry = new THREE.ExtrudeGeometry( this.zoneShape, extrudeSettings );
			this.objMeshNav.push(new THREE.Mesh( zonegeometry, this.zoneMaterial ));
			this.glScene.add(this.objMeshNav[this.objMeshNav.length - 1]);

			// Free geometry memory
			zonegeometry.dispose();
		}

		// Clear local memory
		gl_clear_obj_memory(extrudePath);
		gl_clear_obj_memory(extrudeSettings);

	},
	init_mesh_pedestrian: function (self)
	{
		// Add Pedestrian
		self.loaderCollada.load(MVT_VISUALIZER.pedestrian.url, function ( collada ) 
		{
			for( var i = 0; i < MVT_VISUALIZER.config.numPedRecv; i++ )
			{
				var newPed = collada.scene.clone();
				newPed.skin = collada.skins[0];
				newPed.scale.x = newPed.scale.y = newPed.scale.z = MVT_VISUALIZER.config.scalePed; 
				newPed.rotation.x = -Math.PI / 2;
				newPed.position.y = 0;
				newPed.position.x = 0;
				newPed.position.z = 0;
				newPed.visible = false;

				newPed.traverse( function ( child ) 
				{
					if ( child instanceof THREE.SkinnedMesh ) 
					{
						self.animPed.push(new THREE.Animation( child, child.geometry.animation ));
						self.animPed[self.animPed.length - 1].play(0.4);
					}
				});

				self.objMeshPed.push(newPed);
				self.glScene.add( self.objMeshPed[self.objMeshPed.length - 1] ); 
				gl_clear_obj_memory(newPed);
			}
			gl_clear_obj_memory(collada);
		});
	},
	init_renderer: function (self)
	{
	    // Init renderer parameter
	    self.renderer.setPixelRatio( window.devicePixelRatio );
	    self.renderer.setSize( window.innerWidth, window.innerHeight );
	    self.renderer.setFaceCulling( THREE.CullFaceNone );
	    self.renderer.autoClear = true;
	    self.container.append( self.renderer.domElement );

	},
	initSkyBox: function (self) 
	{
		// Add skybox
		self.textureCube = new THREE.CubeTextureLoader().load( self.skyboxUrl );
		self.textureCube.format = THREE.RGBFormat;
        self.glScene.background = self.textureCube;

		// Add ambient light
		var ambient = new THREE.AmbientLight( 0x050505, 50 );
		self.glScene.add( ambient );

		var ww = self.worldWidth;
		var wh = self.worldHeight;
		var light = new THREE.DirectionalLight( 0xdfebff, 1.75 );
		light.castShadow = false;
		light.shadow.mapSize.width = ww;
		light.shadow.mapSize.height = wh;
		light.shadow.camera.left = -ww;
		light.shadow.camera.right = ww;
		light.shadow.camera.top = ww;
		light.shadow.camera.bottom = -ww;
		light.shadow.camera.far = wh;
		self.glScene.add( light );

		// Add direct light
		var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.1 );
		directionalLight.position.set( 100, 100, 100 ).normalize();
		self.glScene.add( directionalLight );

		// Init material library
		self.init_material_lib(self);
	},
    init_shape_file: function (self, shpPath, shpColor, lineWidth)
    {
        var tmpX, tmpY;
        shapefile.open(shpPath, null)
            .then(function(source) 
            {
                return source.read().then(function next(result) 
                {
                    if (result.done) return;

                    var geometry = new THREE.Geometry();
			        for(var i = 0; i < result.value.geometry.coordinates.length; i++)
			        {
				        tmpX = (parseFloat(result.value.geometry.coordinates[i][1]) - MVT_VISUALIZER.config.globalOffsetX);
				        tmpY = (parseFloat(result.value.geometry.coordinates[i][0]) - MVT_VISUALIZER.config.globalOffsetY);
				        if(tmpX < self.curbMinX) self.curbMinX = tmpX;
				        if(tmpX > self.curbMaxX) self.curbMaxX = tmpX;
				        if(tmpY < self.curbMinY) self.curbMinY = tmpY;
				        if(tmpY > self.curbMaxY) self.curbMaxY = tmpY;
				        geometry.vertices.push(new THREE.Vector3(tmpX, 0, tmpY));
			        }

                    var line = new MeshLine();
                    line.setGeometry( geometry );
                    var material = new MeshLineMaterial({color: new THREE.Color(shpColor), lineWidth: lineWidth});
                    var mesh = new THREE.Mesh( line.geometry, material );
                    self.glScene.add( mesh );
        
                    return source.read().then(next);
                });
            })
            .catch(function(error) {
                console.error(error.stack);
            });
    },
    rebind_notification: function()
    {
        $("#panel-top-notify .notify-icon-x").unbind("click touch");
        $("#panel-top-notify .notify-icon-x").bind("click touch", function() {
            $(this).parent("li").hide("slide", {
                direction: "right"
            }, 500, function(){$(this).remove();});
        });
    },
	update_dyo: function (ind, posX, posZ, rotZ, ol, ow, oh)
	{
	    // Update dynamic obstacle wireframe position and orientation
	    this.objMeshObjWf[ind].position.x = posX;
        this.objMeshObjWf[ind].position.y = 0;
	    this.objMeshObjWf[ind].position.z = posZ;
	    this.objMeshObjWf[ind].rotation.y = rotZ;
        this.objMeshObjWf[ind].scale.x = ow;
        this.objMeshObjWf[ind].scale.y = oh;
        this.objMeshObjWf[ind].scale.z = ol;
        this.objMeshObjWf[ind].visible = true;
        this.objMeshObjWf[ind].needUpdate = true;
	},
	update_ego: function (posX, posZ, rotY, speed)
	{
		// Update ego position and orientation
		this.objMesh[this.avIndex].rotation.y = rotY;
		this.objMesh[this.avIndex].position.x = posX;
	 	this.objMesh[this.avIndex].position.z = posZ;
	
		// Update brake light indicator
		if( this.avSpeed - speed > 0.001 || speed < 0.5)
		{
			if( MVT_VISUALIZER.config.carEgo == "alphard" )
				this.objMesh[this.avIndex].material.materials[8] = this.objMesh[this.avIndex].material.materials[9] = this.mlib["Red glass"];
			else if( MVT_VISUALIZER.config.carEgo == "veyron" )
				this.objMesh[this.avIndex].material.materials[6] = this.mlib["Red glass"];
            else if( MVT_VISUALIZER.config.carEgo == "gallardo" )
				this.objMesh[this.avIndex].material.materials[4] = this.mlib["Red glass"];
		}
		else
		{
			if( MVT_VISUALIZER.config.carEgo == "alphard" )
				this.objMesh[this.avIndex].material.materials[8] = this.objMesh[this.avIndex].material.materials[9] = this.mlib["Red"];
			else if( MVT_VISUALIZER.config.carEgo == "veyron" )
				this.objMesh[this.avIndex].material.materials[6] = this.mlib["Red"];
            else if( MVT_VISUALIZER.config.carEgo == "gallardo" )
				this.objMesh[this.avIndex].material.materials[4] = this.mlib["Red"];
		}

		// Update mesh
		this.objMesh[this.avIndex].needUpdate = true;

		// Update speed meter
		this.avSpeed = speed;
        MY_VISUALIZER.gauge.set(this.avSpeed);
	},
	update_mdf: function (inputStr)
	{
		// Clear previous obj & release previous memory
		for(var i = this.objMeshMdf.length - 1; i >= 0; i--)
		{
			this.glScene.remove(this.objMeshMdf[i]);
			gl_clear_obj_memory(this.objMeshMdf[i]);
		}

		// Process mdf waypoints
		var splitMsg = inputStr.split(";");
		if(splitMsg.length > 1)
		{
			var pos, j;
			var s = MVT_VISUALIZER.config.scaleSto;

			// Create sprite particle
			var cTexture = new THREE.CanvasTexture( this.gl_generate_sprite('rgba(64,0,64,1)') );
			var material = new THREE.SpriteMaterial( {
				map: cTexture,
				blending: THREE.AdditiveBlending
			} );

			for(var i = 0; i < splitMsg.length - 1; i++)
			{
				// Set mdf point into map
				pos = splitMsg[i].split(",");
				this.objMeshMdf.push(new THREE.Sprite( material ));
				j = this.objMeshMdf.length - 1;
				this.objMeshMdf[j].scale.set(s, s, s);

				this.objMeshMdf[j].position.y = 0.1;
                if (MVT_VISUALIZER.config.isXYAxisRevert)
                {
				    this.objMeshMdf[j].position.x = parseFloat(pos[1]) - MVT_VISUALIZER.config.globalOffsetX;
				    this.objMeshMdf[j].position.z = parseFloat(pos[0]) + MVT_VISUALIZER.config.globalOffsetY;
                }
                else
                {
				    this.objMeshMdf[j].position.x = parseFloat(pos[0]) - MVT_VISUALIZER.config.globalOffsetX;
				    this.objMeshMdf[j].position.z = parseFloat(pos[1]) + MVT_VISUALIZER.config.globalOffsetY;
                }
				this.glScene.add(this.objMeshMdf[j]);
			}

			// Clear memory
			gl_clear_obj_memory(material);
			gl_clear_obj_memory(cTexture);
		}
	},
	update_nav: function (inputStr)
	{
		var splitMsg = inputStr.split(";");

        /// Update box for distToSlow
        pSlow = splitMsg[splitMsg.length - 1].split(",");
        if (MVT_VISUALIZER.config.isXYAxisRevert)
        {
            var pSlowX = parseFloat(pSlow[1]) - MVT_VISUALIZER.config.globalOffsetX;
            var pSlowZ = parseFloat(pSlow[0]) + MVT_VISUALIZER.config.globalOffsetY;
        }
        else
        {
            var pSlowX = parseFloat(pSlow[0]) - MVT_VISUALIZER.config.globalOffsetX;
            var pSlowZ = parseFloat(pSlow[1]) + MVT_VISUALIZER.config.globalOffsetY;
        }
        var rotSlowY = parseFloat(pSlow[2]);

        // Update slow estimate slow zone
        if (pSlowX == -1 && pSlowZ == -1)
            MY_VISUALIZER.hide_dyo(0);
        else
            MY_VISUALIZER.update_dyo(0, pSlowX, pSlowZ, rotSlowY, 2, 10, 2);

        // Update local path planning zone
		var zs = MVT_VISUALIZER.config.zoneStep;
		if(splitMsg.length > zs && (this.displayOptions[1] || this.displayOptions[2]))
		{

		    var v1, pos;
		    var numToAppend = splitMsg.length - this.objMeshNav.length;
	
		    // Init mesh if required
		    if(numToAppend > 0)
			    this.init_mesh_nav(numToAppend);

		    // Update rectangle mesh position
		    var i, ii, iii;
		    var v1 = new THREE.Vector3(0, 0.05, 0);
		    var v2 = new THREE.Vector3(0, 0.05, 0);
            var colorDiffR = MVT_VISUALIZER.config.colorStop[0] - MVT_VISUALIZER.config.colorGo[0];
            var colorDiffG = MVT_VISUALIZER.config.colorStop[1] - MVT_VISUALIZER.config.colorGo[1];
            var speedDiff = MVT_VISUALIZER.config.speedSlow - MVT_VISUALIZER.config.speedStop;
		    var extrudePath;
		    for(i = zs + 1; i < splitMsg.length - 2; i+= zs)    // -2 for distoslow, change to -1 if disable distoslow
		    {
			    ii = i - zs;
			    pos1 = splitMsg[ii].split(",");
			    pos2 = splitMsg[i].split(",");
                if (MVT_VISUALIZER.config.isXYAxisRevert)
                {
			        v1.x = parseFloat(pos1[1]) - MVT_VISUALIZER.config.globalOffsetX;
			        v1.z = parseFloat(pos1[0]) + MVT_VISUALIZER.config.globalOffsetY;
			        v2.x = parseFloat(pos2[1]) - MVT_VISUALIZER.config.globalOffsetX;
			        v2.z = parseFloat(pos2[0]) + MVT_VISUALIZER.config.globalOffsetY;
                }
                else
                {
			        v1.x = parseFloat(pos1[0]) - MVT_VISUALIZER.config.globalOffsetX;
			        v1.z = parseFloat(pos1[1]) + MVT_VISUALIZER.config.globalOffsetY;
			        v2.x = parseFloat(pos2[0]) - MVT_VISUALIZER.config.globalOffsetX;
			        v2.z = parseFloat(pos2[1]) + MVT_VISUALIZER.config.globalOffsetY;
                }
			    v1 = get_abs_pos(this.objMesh[this.avIndex].rotation.y, v1, this.zoneOffset);
			    v2 = get_abs_pos(this.objMesh[this.avIndex].rotation.y, v2, this.zoneOffset);

			    extrudePath = new THREE.LineCurve3( v1, v2 );
			    iii = Math.floor(i / zs);
			    gl_dispose_memory(this.objMeshNav[iii]);
			    this.objMeshNav[iii].geometry = new THREE.ExtrudeGeometry( this.zoneShape, {bevelEnabled: false, steps: 1, extrudePath: extrudePath} );
                
                /// Set zone color based on speed given
                var refSpd = parseFloat(pos1[2]);
                var r, g, b;
                if (refSpd < MVT_VISUALIZER.config.speedStop)
                {
                    r = MVT_VISUALIZER.config.colorStop[0];
                    g = MVT_VISUALIZER.config.colorStop[1];
                    b = MVT_VISUALIZER.config.colorStop[2];
                }
                else if (refSpd < MVT_VISUALIZER.config.speedSlow)
                {
                    r = MVT_VISUALIZER.config.colorGo[0] + ((MVT_VISUALIZER.config.speedSlow - refSpd) / speedDiff * colorDiffR);
                    g = MVT_VISUALIZER.config.colorGo[1] + ((MVT_VISUALIZER.config.speedSlow - refSpd) / speedDiff * colorDiffG);
                    b = MVT_VISUALIZER.config.colorSlow[2];
                }
                else
                {
                    r = MVT_VISUALIZER.config.colorGo[0];
                    g = MVT_VISUALIZER.config.colorGo[1];
                    b = MVT_VISUALIZER.config.colorGo[2];                   
                }
                var color = new THREE.Color( r, g, b );
                this.objMeshNav[iii].material = new THREE.MeshLambertMaterial({color: color, opacity: 0.55, transparent: true});
                
                /// Clear memory
                gl_clear_obj_memory(color);
                gl_clear_obj_memory(extrudePath);

                if(this.displayOptions[2])
			        this.objMeshNav[iii].visible = true;
                else
                    this.objMeshNav[iii].visible = false;

                this.objMeshNav[iii].needUpdate = true;

			    if(i + zs >= splitMsg.length - 1)
				    gl_clear_obj_memory(pos1);
			    else
			    {			
				    gl_clear_obj_memory(pos1);
				    gl_clear_obj_memory(pos2);
			    }
		    }
            
            // Record valid mesh number
            this.lineMeshNavPrevLength = i;

		    // Hide unused mesh
		    for(var j = i; j < this.objMeshNav.length; j++)
		    {
		        if(!this.objMeshNav[j].visible)
			        break;
		        else
			        this.objMeshNav[j].visible = false;
		    }

		    gl_clear_obj_memory(v1);
		    gl_clear_obj_memory(v2);

            // Update local path line for inspector
            // Clear previous line mesh memory
            this.glScene.remove(this.lineMeshNav);
            gl_clear_obj_memory(this.lineMeshNav);

            // Declaration
            var vec ,pos;
            var navGeo = new THREE.Geometry();
            
            // Append geometry
            for(var i = 0; i < splitMsg.length - 1; i++)
	        {
                pos = splitMsg[i].split(",");
                vec = new THREE.Vector3(0, 0.6, 0);
                if (MVT_VISUALIZER.config.isXYAxisRevert)
                {
		            vec.x = parseFloat(pos[1]) - MVT_VISUALIZER.config.globalOffsetX;
		            vec.z = parseFloat(pos[0]) + MVT_VISUALIZER.config.globalOffsetY;
                }
                else
                {
		            vec.x = parseFloat(pos[0]) - MVT_VISUALIZER.config.globalOffsetX;
		            vec.z = parseFloat(pos[1]) + MVT_VISUALIZER.config.globalOffsetY;
                }
                navGeo.vertices.push(vec);
                gl_clear_obj_memory(vec);
            }

            // Generate line mesh
            if(navGeo.vertices.length > 0)
            {
                this.lineMeshNav = new THREE.Line(navGeo, this.lineMeshNavMat);

                if(this.displayOptions[1])
			        this.lineMeshNav.visible = true;
                else
                    this.lineMeshNav.visible = false;

                this.lineMeshNav.needUpdate = true;
                this.glScene.add(this.lineMeshNav);
            }

            // Clear memory
            gl_clear_obj_memory(navGeo);
		}
	},
	update_ped: function (ind, posX, posZ, rotZ)
	{
		// Update pedestrian position and orientation
		this.objMeshPed[ind].position.x = posX;
		this.objMeshPed[ind].position.z = posZ;
		this.objMeshPed[ind].rotation.z = rotZ;
		this.objMeshPed[ind].visible = true;
		this.objMeshPed[ind].needUpdate = true;
	},
	update_resize: function ()
	{
		// Change rendering threejs size
		windowHalfX = window.innerWidth / 2;
		windowHalfY = window.innerHeight / 2;

		// Update camera matrix
		this.camera.aspect = window.innerWidth / window.innerHeight;
		this.camera.updateProjectionMatrix();

		// Update cameraCube matrix
		this.cameraCube.aspect = window.innerWidth / window.innerHeight;
		this.cameraCube.updateProjectionMatrix();

		// Update renderer matrix
		this.renderer.setSize( window.innerWidth, window.innerHeight); 
	},
	update_sto: function (ind, posX, posZ, isDynamic)
	{
        if (!isDynamic)
        {
		    // Update blue dots
		    this.objMeshSto[ind].position.x = posX;
		    this.objMeshSto[ind].position.z = posZ;
		    this.objMeshSto[ind].visible = true;
		    this.objMeshSto[ind].needUpdate = true;
        }
        else
        {
		    // Update red dots
		    this.objMeshDyo[ind].position.x = posX;
		    this.objMeshDyo[ind].position.z = posZ;
		    this.objMeshDyo[ind].visible = true;
		    this.objMeshDyo[ind].needUpdate = true;
        }
	},
	update_veh: function (ind, posX, posZ, rotZ, vl, vw)
	{
	    // Update vehicles position and orientation
	    this.objMeshVeh[ind].position.x = posX;
        this.objMeshVeh[ind].position.y = 0;
	    this.objMeshVeh[ind].position.z = posZ;
	    this.objMeshVeh[ind].rotation.y = rotZ;

        if( this.displayOptions[4] )
	        this.objMeshVeh[ind].visible = true;
        else
            this.objMeshVeh[ind].visible = false;
        this.objMeshVeh[ind].needUpdate = true;

	    // Update vehicles wireframe position and orientation
	    this.objMeshVehWF[ind].position.x = posX;
        this.objMeshVehWF[ind].position.y = 0;
	    this.objMeshVehWF[ind].position.z = posZ;
	    this.objMeshVehWF[ind].rotation.y = rotZ;
        this.objMeshVehWF[ind].scale.x = vw;
        this.objMeshVehWF[ind].scale.z = vl;

        if( this.displayOptions[3] )
	        this.objMeshVehWF[ind].visible = true;
        else
            this.objMeshVehWF[ind].visible = false;
        this.objMeshVehWF[ind].needUpdate = true;
	}
};


/*******************************************************************************
 * Websocket prototypes
 ******************************************************************************/
function init_websocket()
{
	if ("WebSocket" in window)
	{
        var steeringRotateDeg = 0;
		WS = new WebSocket("ws://" + MVT_VISUALIZER.config.webSocketIP);
		WS.onopen = function()
		{
			// Web Socket is connected, initialize
			MY_VISUALIZER.isWebSocketOpen = true;

			// Close timeout if exist
			clearTimeout(WSTIMEOUT);
		};

		WS.onclose = function()
		{ 
			// Websocket is closed
			MY_VISUALIZER.isWebSocketOpen = false;
		
			// Reconnect via timeout
			WSTIMEOUT = setTimeout(init_websocket, 1000); 
		};

		WS.onmessage = function( evt )
		{ 
            jsonMsg_ = JSON.parse(evt.data);
            if (jsonMsg_.type == "mooVisualizer")
            {
                var recvMsg = jsonMsg_.data.split("[BRK]");
                if (recvMsg[0] == "s1")
                {
                    var splitMsg = recvMsg[1].split(",");
                    
                    if (splitMsg.length > 0 && splitMsg[0] == '1')
                    {
                        if (MY_HMI._globStatus == 0)
                        {
                            MY_HMI._globStatus = 2;
                            btn_start_on('btn-start-all');
                        }
                    }
                    else
                    {
                        if (MY_HMI._globStatus == 2)
                        {
                            MY_HMI._globStatus = 0;
                            btn_start_off('btn-start-all');
                        }
                    }
                                        
                    if (splitMsg.length > 1 && splitMsg[1] == '1')
                        window.parent.$('#s1-dm .view-info-button').addClass('on')
                    else
                        window.parent.$('#s1-dm .view-info-button').removeClass('on')
                        
                    if (splitMsg.length > 2 && splitMsg[2] == '1')
                        window.parent.$('#s1-mp .view-info-button').addClass('on')
                    else
                        window.parent.$('#s1-mp .view-info-button').removeClass('on')
                        
                    if (splitMsg.length > 3 && splitMsg[3] == '1')
                        window.parent.$('#s1-cod .view-info-button').addClass('on')
                    else
                        window.parent.$('#s1-cod .view-info-button').removeClass('on')
                        
                    if (splitMsg.length > 3 && splitMsg[4] == '1')
                        window.parent.$('#s1-lod .view-info-button').addClass('on')
                    else
                        window.parent.$('#s1-lod .view-info-button').removeClass('on')    
                }
                else if( recvMsg[0] == "gps" && MY_VISUALIZER.avIndex >= 0 )
                {
                    // Retrieve vehicle state data
                    var splitMsg = recvMsg[1].split(",");
                    if (MVT_VISUALIZER.config.isXYAxisRevert)
                    {
                        var posX = parseFloat(splitMsg[1]) - MVT_VISUALIZER.config.globalOffsetX;
                        var posZ = parseFloat(splitMsg[0]) + MVT_VISUALIZER.config.globalOffsetY;
                    }
                    else
                    {
                        var posX = parseFloat(splitMsg[0]) - MVT_VISUALIZER.config.globalOffsetX;
                        var posZ = parseFloat(splitMsg[1]) + MVT_VISUALIZER.config.globalOffsetY;
                    }
                    var rotY = parseFloat(splitMsg[2]);
                    var speed = parseFloat(splitMsg[3]);
                    var d2s = parseInt(splitMsg[4]);
                    var dm = parseInt(splitMsg[8]);

                    // Update car position and orienation
                    MY_VISUALIZER.update_ego(posX, posZ, rotY, speed);

                    // Update driving mode status
                    if (dm == 0)
                        $("#drive-mode").html('Manual').css({'background-color': 'mediumseagreen', 'display': 'block'});
                    else if(dm == 2)
                        $("#drive-mode").html('Autonomous').css({'background-color': 'cornflowerblue', 'display': 'block'});
                    else if (dm == 4)
                        $("#drive-mode").html('Idle').css({'background-color': 'crimson', 'display': 'block'});
                    else if (dm == 8)
                        $("#drive-mode").html('E-stop').css({'background-color': 'darkcyan', 'display': 'block'});
                    else
                        $("#drive-mode").css('display', 'none');


                    // Update vehicle state info
                    $("#vs-gauge-val").html(Math.round(speed));

                    // Update gauge
                    MY_VISUALIZER.gauge.set(speed.toFixed(2) * 10);
                    
                    // Update remain distance information
                    var spdEta_ = ((speed / 3.6) > MY_VISUALIZER.minSpeedEstimate) ? (speed / 3.6) : MY_VISUALIZER.minSpeedEstimate; 
                    var timeEta_ = parseFloat(d2s) / spdEta_;
                    if (timeEta_ > 60)
                        $("#dest-info li:first-child").html(Math.round(timeEta_ / 60) + " mins");
                    else
                        $("#dest-info li:first-child").html((Math.round(parseFloat(d2s) / spdEta_)) + " seconds");
                    $("#dest-info li:nth-child(2)").html(d2s + "m");

                    // Update vehicle indicators
                    /*
                    if (di == 0)
                        $("#di-l, #di-r").removeClass("on");
                    else if (di == 1)
                    {
                        $("#di-l").addClass("on");
                        $("#di-r").removeClass("on");
                    }
                    else if (di == 1)
                    {
                        $("#di-r").addClass("on");
                        $("#di-l").removeClass("on");
                    }
                    else
                        $("#di-l, #di-r").addClass("on");

                    if (rv == 1)
                        $("#rv").addClass("on");
                    else
                        $("#rv").removeClass("on");
                    */
                    
                }
                else if( recvMsg[0] == "ped" )
                {
                    // Segment received message
                    var splitMsg = recvMsg[1].split(";");
                    var pedCount = splitMsg.length - 1;
                    if( pedCount > 0 )
                    {
                        // Loop pedestrian pos
                        for(var i = 0; i < pedCount; i++)
                        {
                            // Guard to control worst case
                            if( i >= MVT_VISUALIZER.config.numPedRecv )
                                break;

                            var pos = splitMsg[i].split(",");

                            if (MVT_VISUALIZER.config.isXYAxisRevert)
                            {
                                var posX = parseFloat(pos[1]) - MVT_VISUALIZER.config.globalOffsetX;
                                var posZ = parseFloat(pos[0]) + MVT_VISUALIZER.config.globalOffsetY;
                            }
                            else
                            {
                                var posX = parseFloat(pos[0]) - MVT_VISUALIZER.config.globalOffsetX;
                                var posZ = parseFloat(pos[1]) + MVT_VISUALIZER.config.globalOffsetY;
                            }
                            var rotZ = parseFloat(pos[2]);
                            
                            // Update pedestrian position and orienation
                            MY_VISUALIZER.update_ped( i, posX, posZ, rotZ );
                        }
                    }

                    // Hide usued pedestrian
                    MY_VISUALIZER.hide_ped( pedCount );

                    // Display ped number at sidebar
                    if(pedCount > 0)
                    {
                        // Animate veh icon
                        if (!$("#info-ped").prev().hasClass("animated"))
                            $("#info-ped").prev().attr("src", "images/pedestrian-ico@3x.png").addClass("animated flash")
                    }
                    else
                    {
                        /// Remove veh icon
                        if ($("#info-ped").prev().hasClass("animated"))
                            $("#info-ped").prev().attr("src", "images/pedestrian-ico-on@3x.png").removeClass("animated flash") 
                    }
                    $("#info-ped").html(pedCount); 
                }
                else if( recvMsg[0] == "mdf" )
                {
                    // Update global path waypoints
                    MY_VISUALIZER.update_mdf( recvMsg[1] );
                }
                else if(recvMsg[0] == "nav")
                {
                    // Update local path waypoints
                    MY_VISUALIZER.update_nav( recvMsg[1] );
                }
                else if( recvMsg[0] == "park" )
                {
                    // Remove previous box
                    MY_VISUALIZER.glScene.remove( MY_VISUALIZER.parkingLine );
                    var geometry = new THREE.Geometry();
                    var splitMsg = recvMsg[1].split(";");
                    var counter = splitMsg.length - 1;
                    var firstPX, firstPZ;                    
                    for( var i = 0; i < counter; i++ )
                    {
                        var pos = splitMsg[i].split(",");
                        if (MVT_VISUALIZER.config.isXYAxisRevert)
                        {
                            var posX = parseFloat(pos[1]) - MVT_VISUALIZER.config.globalOffsetX;
                            var posZ = parseFloat(pos[0]) + MVT_VISUALIZER.config.globalOffsetY;
                        }
                        else
                        {
                            var posX = parseFloat(pos[0]) - MVT_VISUALIZER.config.globalOffsetX;
                            var posZ = parseFloat(pos[1]) + MVT_VISUALIZER.config.globalOffsetY;
                        }
                    
                        var rotZ = parseFloat(pos[2]);
                        geometry.vertices.push(new THREE.Vector3(posX, 0.0, posZ));

                        if (i == 0)
                        {
                            firstPX = posX;
                            firstPZ = posZ;
                        }
                    }
                    geometry.vertices.push(new THREE.Vector3(firstPX, 0.0, firstPZ));

                    if (parseInt(splitMsg[counter]) == 0) 
		                MY_VISUALIZER.parkingLine = new THREE.Line(geometry, MY_VISUALIZER.parkingLineDetMat);
                    else
                        MY_VISUALIZER.parkingLine = new THREE.Line(geometry, MY_VISUALIZER.parkingLineConMat);
		            MY_VISUALIZER.glScene.add( MY_VISUALIZER.parkingLine );

                }
                else if( recvMsg[0] == "veh" )
                {
                    // Segment received message
                    var splitMsg = recvMsg[1].split(";");
                    var vehCount = splitMsg.length - 1;
                    if( vehCount > 0 )
                    {
                        // Loop vehicle pos
                        for( var i = 0; i < vehCount; i++ )
                        {
                            // Guard to control worst case
                            if( i >= MVT_VISUALIZER.config.numVehRecv )
                                break;

                            var pos = splitMsg[i].split(",");
                            if (MVT_VISUALIZER.config.isXYAxisRevert)
                            {
                                var posX = parseFloat(pos[1]) - MVT_VISUALIZER.config.globalOffsetX;
                                var posZ = parseFloat(pos[0]) + MVT_VISUALIZER.config.globalOffsetY;
                            }
                            else
                            {
                                var posX = parseFloat(pos[0]) - MVT_VISUALIZER.config.globalOffsetX;
                                var posZ = parseFloat(pos[1]) + MVT_VISUALIZER.config.globalOffsetY;
                            }
                            var rotZ = parseFloat(pos[2]);
                            var vl = parseFloat(pos[3]);
                            var vw = parseFloat(pos[4]);
                            
                            // Update vehicle position and orienation
                            MY_VISUALIZER.update_veh( i, posX, posZ, rotZ, vl, vw );
                        }
                    }

                    // Hide usued vehicles
                    MY_VISUALIZER.hide_veh( vehCount );

                    // Display veh number at sidebar
                    if(vehCount > 0)
                    {
                        // Animate veh icon
                        if (!$("#info-veh").prev().hasClass("animated"))
                            $("#info-veh").prev().attr("src", "images/vehicle-ico-on@3x.png").addClass("animated flash")
                    }
                    else
                    {
                        /// Remove veh icon
                        if ($("#info-veh").prev().hasClass("animated"))
                            $("#info-veh").prev().attr("src", "images/vehicle-ico@3x.png").removeClass("animated flash")  
                    }
                    $("#info-veh").html(vehCount);    
                }
                else if( recvMsg[0] == "sto" )
                {
                    // Segment received message
                    var numSto = 0;
                    var numDyo = 0;
                    var numDyoP = 0;
                    var splitMsg = recvMsg[1].split(";");
                    var stoCount = splitMsg.length - 1;
                    if( stoCount > 0 )
                    {
                        // Loop static obstacle points
                        for(var i = 0; i < stoCount; i++)
                        {
                            if (i >= MVT_VISUALIZER.config.numStaticObjRecv)
                                break;

                            var pos = splitMsg[i].split(",");

                            if (MVT_VISUALIZER.config.isXYAxisRevert)
                            {
                                var posX = parseFloat(pos[1]) - MVT_VISUALIZER.config.globalOffsetX;
                                var posZ = parseFloat(pos[0]) + MVT_VISUALIZER.config.globalOffsetY;
                            }
                            else
                            {
                                var posX = parseFloat(pos[0]) - MVT_VISUALIZER.config.globalOffsetX;
                                var posZ = parseFloat(pos[1]) + MVT_VISUALIZER.config.globalOffsetY;
                            }
                            var rotZ = parseFloat(pos[2]);
                            var os = parseFloat(pos[3]);
                            var ol = parseFloat(pos[4]);
                            var ow = parseFloat(pos[5]); 
                            var oh = parseFloat(pos[6]); 

                            // Update pedestrian position and orienation
                            if (Math.abs(os) <= MVT_VISUALIZER.config.stoStop)
                            {
                                MY_VISUALIZER.update_sto( numSto, posX, posZ, 0 );
                                numSto ++;
                            }
                            else if (ol == 0.1 && ow == 0.1 && oh == 0.1)
                            {
                                MY_VISUALIZER.update_sto( numDyoP, posX, posZ, 1 );
                                numDyoP ++;
                            }
                            else 
                            {
                                MY_VISUALIZER.update_dyo( numDyo, posX, posZ, rotZ, ol, ow, oh );
                                numDyo ++;
                            }                        
                        }
                        
                        // Animate sto icon
                        if (!$("#info-obs").prev().hasClass("animated"))
                            $("#info-obs").prev().attr("src", "images/obstacle-ico-on@3x.png").addClass("animated flash")
                    }
                    else
                    {
                        // Remove icon light
                        if ($("#info-obs").prev().hasClass("animated"))
                            $("#info-obs").prev().attr("src", "images/obstacle-ico@3x.png").removeClass("animated flash")                    
                    }

                    // Hide usued static/dynamic obstacles
                    MY_VISUALIZER.hide_sto( numSto );
                    MY_VISUALIZER.hide_dyo_p( numDyoP );
                    //MY_VISUALIZER.hide_dyo( numDyo );

                    // Display obs number
                    $("#info-obs").html(stoCount);

                }
                else if(recvMsg[0] == "status")
                {
                    // Update loading status when complete
                    LOAD_STATUS = parseInt(recvMsg[1]);
                }
                else if(recvMsg[0] == "trl")
                {
                    var splitMsg = recvMsg[1].split(",");
                    var trlState = parseInt(splitMsg[0]);
                    var trlrState = parseInt(splitMsg[1]);
                    var trsNum = parseInt(splitMsg[2]);
                
                    // Display traffic light state at sidebar
                    if(trlState < 3)
                    {
                        if(!$("#per-trl").hasClass("show"))
                            $("#per-trl").addClass("show");

                        $("#per-trl span").removeClass('stop slow go');
                        if(trlState == 0)
                        {
                            $("#per-trl span:nth-child(2)").addClass("stop");
                            $("#per-trl span:nth-child(3), #per-trl span:nth-child(4)").removeClass('stop slow go');
                        }
                        else if(trlState == 1)
                        {
                            $("#per-trl span:nth-child(3)").addClass("slow");
                            $("#per-trl span:nth-child(2), #per-trl span:nth-child(4)").removeClass('stop slow go');
                        }
                        else if(trlState == 2)
                        {
                            $("#per-trl span:nth-child(4)").addClass("go");
                            $("#per-trl span:nth-child(2), #per-trl span:nth-child(3)").removeClass('stop slow go');
                        }
                    }
                    else
                    {
                        $("#per-trl span").removeClass('stop slow go');
                        $("#per-trl").removeClass("show");
                    }

                    // Display traffic light arrow state at sidebar
                    if(trlrState > 0)
                    {
                        $("#trlr i").removeClass("fa-arrow-left fa-arrow-right")
                        $("#trlr").addClass("go").css('cssText', 'display: inline-block !important;');
                        
                        if (trlrState == 1)
                            $("#trlr i").addClass("fa-arrow-right");
                        else if (trlrState == 2)
                            $("#trlr i").addClass("fa-arrow-left");

                        if(!$("#per-trl").hasClass("trlr"))
                            $("#per-trl").addClass("trlr");
                    }
                    else
                    {
                        $("#per-trl").removeClass("trlr");
                        $("#trlr").removeClass('go').css("display", "none");
                    }

                    // Display traffic sign state
                    if(trsNum > 0 && splitMsg.length >= trsNum + 3)
                    {
                        $("#per-trs span").html(trsNum);

                        var imgCount = $("#per-trs div img").length;
                        for( i = 0; i < trsNum; i++ )
                        {
                            if(i < imgCount)
                                $("#per-trs div img").eq(i + 1).attr("src", 'images/"+ splitMsg[i + 3] +".png');
                            else
                                $("#per-trs div").append("<img src='images/"+ splitMsg[i + 3] +".png' />");
                        }

                        if(!$("#per-trs").hasClass("show"))
                            $("#per-trs").addClass("show");

                        $("#per-trs div").css("display", 'block');
                    }
                    else
                    {
                        $("#per-trs span, #per-trs div").html('');
                        $("#per-trs div").css("display", 'none');
                        $("#per-trs").removeClass("show");
                    }
                }
            }
            else if (jsonMsg_.type == "pod_status")
            {
                // Common debug info
                $("#l1-seq, #db-seq span:last-of-type").html(jsonMsg_.data.seq);
                $("#l1-mode, #db-mode span:last-of-type, #vs-ego-mode div").html((jsonMsg_.data.isAuto == '0') ? "Manual" : "Auto" );
                $("#l1-state, #db-state span:last-of-type").html(jsonMsg_.data.PodState);
                $("#l1-vel, #db-vel span:last-of-type").html(Math.round(parseFloat(jsonMsg_.data.Velocity)) + "km/h");
                $("#l1-rv, #db-rv span:last-of-type").html(Math.round(parseFloat(jsonMsg_.data.ReqSpeed)) + "km/h");
                $("#l1-rs, #db-rs span:last-of-type").html(jsonMsg_.data.ReqSteering + "%");
                $("#l1-es, #db-es span:last-of-type").html((jsonMsg_.data.EStop) ? "True" : "False");
                $("#l1-rf-es, #db-rf-es span:last-of-type").html((jsonMsg_.data.RfEstopTriggered) ? "True" : "False");
                $("#l1-bs, #db-bs span:last-of-type").html((jsonMsg_.data.ParkBrakeState == '0') ? "False" : "True");
                $("#l1-ld, #db-ld span:last-of-type").html((jsonMsg_.data.LeftDoorClosed) ? "Closed" : "Opened");
                $("#l1-rd, #db-rd span:last-of-type").html((jsonMsg_.data.RightDoorClosed) ? "Closed" : "Opened");
                $("#l1-js, #db-js span:last-of-type").html((jsonMsg_.data.joystick_init) ? "Disconnected" : "Connected");
                $("#l1-ifss, #db-ifss span:last-of-type").html(jsonMsg_.data.isFrontSafeStop);
                $("#l1-irss, #db-irss span:last-of-type").html(jsonMsg_.data.isRearSafeStop);

                // Battery 
                $("#l1-charge, #db-charge span:last-of-type, #info-battery").html(jsonMsg_.data.StateOfCharge + "%"); 
                var percentBattery = parseFloat(jsonMsg_.data.StateOfCharge)
                if (percentBattery < 30)
                    $("#info-battery").prev().attr("src", "images/battery0.png")
                else if (percentBattery >= 30 & percentBattery < 70)
                    $("#info-battery").prev().attr("src", "images/battery1.png")
                else if (percentBattery >= 70 & percentBattery < 89)
                    $("#info-battery").prev().attr("src", "images/battery2.png")
                else
                    $("#info-battery").prev().attr("src", "images/battery3.png")
                    
                // Steering
                var sa = parseFloat(jsonMsg_.data.SteeringPercent);
                $("#l1-steer, #db-steer span:last-of-type, #info-steering").html(sa + "%");
                $("#info-steering").prev().rotate({
                    angle: steeringRotateDeg,
                    animateTo: sa
                })
                steeringRotateDeg = sa;
            }
            else if (jsonMsg_.type == "pod_monitor")
            {
                $("#l2-pmsg, #db-pmsg span:last-of-type").html(jsonMsg_.data.pod_msg.status);
                $("#l2-imsg, #db-imsg span:last-of-type").html(jsonMsg_.data.imu_msg.status);
                $("#l2-lmsg, #db-lmsg span:last-of-type").html(jsonMsg_.data.loc_msg.status);
                $("#l2-omsg, #db-omsg span:last-of-type").html(jsonMsg_.data.odom_msg.status);
                $("#l2-emsg, #db-emsg span:last-of-type").html(jsonMsg_.data.ekf_msg.status);
                $("#l2-tlidar, #db-tlidar span:last-of-type").html(jsonMsg_.data.top_lidar_msg.status);
                $("#l2-blidar, #db-blidar span:last-of-type").html(jsonMsg_.data.bottom_lidar_msg.status);
                $("#l2-flidar, #db-flidar span:last-of-type").html(jsonMsg_.data.fused_lidar_msg.status);
            }
		}
	}
	else
	{
		alert("Websocket not supported. Please update your web browser.");
	}
}


/***********************************************************************
 * Function Prototypes
 **********************************************************************/
/* One button start */
function btn_start_on(id)
{
    $("#" + id).addClass("on");
    $("#" + id + " div:nth-child(2)").html("STOP");
}

/* One button stop */
function btn_start_off(id)
{
    $("#" + id + ", .view-info-button").removeClass("on");
    $("#" + id + " div:nth-child(2)").html("START");
}

/* Get full date now */
function get_date_now()
{
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    return d.getFullYear() + '-' +
    (month<10 ? '0' : '') + month + '-' +
    (day<10 ? '0' : '') + day + " " +
    d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds(); 
}

/* Get full date now */
function notify_msg(_type, _title, _content)
{
    if (!$("#panel-bottom-left li:nth-child(4)").hasClass("on"))
        $("#view-notify-no-read").css("display", "block");
    $("#view-notify-no-message").css("display", "none");
    
    var title_ = _title;
    var content_ = _content;
    var li_ = '<li><div class="notify-icon"><i class="fa fa-exclamation" aria-hidden="true"></i></div><div class="notify-content">'
    li_ = li_ + "<span>" + _title + "</span>";
    li_ = li_ + "<span>" + _content + "</span>";
    
    var li1_ = li_ + '</div><div class="notify-icon-x"><i class="fa fa-times" aria-hidden="true"></i></div></li>';
    $(li1_).prependTo("#panel-top-notify").hide().show("slide", {direction: "right"}, 500, function(){
        MY_VISUALIZER.rebind_notification();
        var this_ = $(this).find(".notify-icon-x");
        setTimeout(function(this_){$(this_).click();}, 5000, this_);
    });

    var li2_ = li_ + '<span>' + get_date_now() + '</span></div></li>';
    $(li2_).prependTo("#view-notify .simplebar-content");  
}

/* View in fullscreen */
function open_full_screen() {
    if (BigScreen.enabled) {
        BigScreen.toggle();
    }
    else
    {
        var elem = document.documentElement;
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.mozRequestFullScreen) { /* Firefox */
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
            elem.webkitRequestFullscreen();
        } else if (elem.msRequestFullscreen) { /* IE/Edge */
            elem.msRequestFullscreen();
        }
    }
}

/* Close fullscreen */
function close_full_screen() {
    if (BigScreen.enabled) {
        BigScreen.toggle();
    }
    else
    {
        var elem = document.documentElement;
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) { /* Firefox */
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
            document.webkitExitFullscreen();
        } else if (document.msExitFullscreen) { /* IE/Edge */
            document.msExitFullscreen();
        }
    }
} 

function init_bind_evt()
{
    /// Bind full screen control
    $("#hmi-icon-full-screen, #vs-btn-full-screen").bind("click touch", function()
    {
        if (MY_HMI._isFullScreen)
        {
            close_full_screen();
            MY_HMI._isFullScreen = 0;
            $("#hmi-icon-full-screen").find("img").attr("src", "images/full_screen_blue_ico.png");
            $("#vs-btn-full-screen").find("img").attr("src", "images/full_screen_white_ico.png");
        }
        else
        {
            open_full_screen();
            MY_HMI._isFullScreen = 1;
            $("#hmi-icon-full-screen").find("img").attr("src", "images/exit_full screen_ico.png");
            $("#vs-btn-full-screen").find("img").attr("src", "images/exit_full screen_white.png");
        }
    });
    
    /// Bind list control at first page
    $("#hmi-info-nav-left, #hmi-info-driver-button-back").bind("click touch", function()
    {
        MY_HMI.preparation_button_prev(MY_HMI);
    });
    $("#hmi-info-nav-right").bind("click touch", function()
    {
        MY_HMI.preparation_button_next(MY_HMI);
    });
    $("#hmi-info-driver-button-start").bind("click touch", function()
    {
        MY_HMI.prepartion_button_start(MY_HMI);
    });
    $("#vs-driver-logout").bind("click touch", function()
    {
        MY_HMI.driver_logout(MY_HMI);
    });
    $('#hmi-info-driver-name').bind("keyup blur", function()
    {
        if( !$(this).val() ) {
            $("#hmi-info-driver-name, #hmi-info-driver-wrapper").addClass("warning");
            MY_HMI.set_driver_valid(MY_HMI, false);
        }
        else
        {
            $("#hmi-info-driver-name, #hmi-info-driver-wrapper").removeClass("warning");
            MY_HMI.set_driver_valid(MY_HMI, true);
        }
    });
    
    /// Bind button function in visualizer
    /// One button start / stop
    $(".vs-btn-power").bind("click touch", function()
    {
        if (MY_VISUALIZER.isWebSocketOpen)
        {
            if ($(this).prop("id") == "btn-start-all")
            {
                if (MY_HMI._globStatus == 0)
                    MY_HMI.start_all(MY_HMI);
                else if (MY_HMI._globStatus == 2)
                    MY_HMI.stop_all(MY_HMI);
            }
            else if ($(this).prop("id") == "btn-start-parking")
            {
                if (MY_HMI._parkStatus == 0)
                    MY_HMI.start_park(MY_HMI);
                else if (MY_HMI._parkStatus == 2)
                    MY_HMI.stop_park(MY_HMI);
            }
            else if ($(this).prop("id") == "btn-start-remote")
            {
                if (MY_HMI._remoteStatus == 0)
                    MY_HMI.start_remote(MY_HMI);
                else if (MY_HMI._remoteStatus == 2)
                    MY_HMI.stop_remote(MY_HMI);
            }
        }
        else
            console.log("Websocket not connected yet, please try again later.");
    });
    
    /// Bind event for module toggle button
    $('#view-module .view-info-button').bind('click touch', function()
    {
        if (MY_VISUALIZER.isWebSocketOpen)
        {
            var id = $(this).parent("li").prop("id");
            MY_HMI.toggle(MY_HMI, id, $(this).hasClass("on"));
        }
        else
            console.log("Websocket not connected yet, please try again later.");
    });
}

function init_progress_circle(){
    /// Init circle at first page for animation
    circle = $('#hmi-rdm-base-progress').circleProgress({
        value: 0.3333,
        size: 1000,
        startAngle: -Math.PI / 2,
        thickness: 50,
        fill: {gradient: ["#25ccb9", "#4985fb"]}
    });

    circle.on('circle-animation-progress', function(e, v) {
        var obj = $(this).data('circle-progress'),
        ctx = obj.ctx,
        s = obj.size,
        sv = (100 * v).toFixed(),
        fill = obj.arcFill;

        ctx.save();
        //ctx.font = "bold " + s / 2.5 + "px sans-serif";
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillStyle = fill;
        ctx.fillText(sv, s / 2,s / 2)
        ctx.beginPath();

        ctx.stroke();
        ctx.restore();
    });    
}

function init_webgl(){
	// Init visualizer main class
	MY_VISUALIZER = new moovita_VIZ();

	// Init animation
	gl_animate();

	// Init websocket
	init_websocket();

    // Window events
    $(window).resize(function() 
    {
	    // Update Webgl matrixes
	    MY_VISUALIZER.update_resize();
        
        // Reset gauge meter
        MY_VISUALIZER.init_gauge(MY_VISUALIZER);

    });
    
    /// Initialize hmi
    window.setTimeout( init_hmi, 1000 ); // wait 1 second
}

function init_hmi(){
    // Init main hmi obj
    MY_HMI = new moovita_HMI();
    
    // Init hmi progress circle
    init_progress_circle();

    // Init bind events
    init_bind_evt();
}


/*******************************************************************************
 * Test prototypes
 ******************************************************************************/
function test_gauge( val ){
    MY_VISUALIZER.gauge.set(val);   
}

function test_notification(){
    if (!$("#panel-bottom-left li:nth-child(4)").hasClass("on"))
        $("#view-notify-no-read").css("display", "block");
    $("#view-notify-no-message").css("display", "none");
    
    var title_ = "Title";
    var content_ = "Content";
    var li_ = '<li><div class="notify-icon"><i class="fa fa-exclamation" aria-hidden="true"></i></div><div class="notify-content">'
    li_ = li_ + "<span>" + title_ + "</span>";
    li_ = li_ + "<span>" + content_ + "</span>";
    
    var li1_ = li_ + '</div><div class="notify-icon-x"><i class="fa fa-times" aria-hidden="true"></i></div></li>';
    $(li1_).prependTo("#panel-top-notify").hide().show("slide", {direction: "right"}, 500, function(){
        MY_VISUALIZER.rebind_notification();
        var this_ = $(this).find(".notify-icon-x");
        setTimeout(function(this_){$(this_).click();}, 5000, this_);
    });
    
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var dt = d.getFullYear() + '-' +
    (month<10 ? '0' : '') + month + '-' +
    (day<10 ? '0' : '') + day + " " +
    d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();;

    var li2_ = li_ + '<span>' + dt + '</span></div></li>';
    $(li2_).prependTo("#view-notify .simplebar-content");
}


/*******************************************************************************
 * Webgl prototypes
 ******************************************************************************/
function gl_animate()
{
	MY_VISUALIZER.animateId = requestAnimationFrame( gl_animate, MY_VISUALIZER.renderer.domElement );
	THREE.AnimationHandler.update( MY_VISUALIZER.clock.getDelta() );

	// Reset pedestrian animation
	for( i = 0; i < MY_VISUALIZER.objMeshPed.length; i++ )
	{
		if( MY_VISUALIZER.objMeshPed[i].visible ) 
		{
			if( MY_VISUALIZER.animPed[i].currentTime > 1.29 ) 
				MY_VISUALIZER.animPed[i].play(0.4);

		}
		else
			break;
	}

	// Render scene
	MY_VISUALIZER.gl_render(MY_VISUALIZER);
}

function gl_clear_obj_memory(obj)
{
	obj = undefined;
	delete(obj);
}

function gl_dispose_memory(obj)
{
	obj.geometry.dispose();
	obj.material.dispose();
    obj = undefined;
}

/*******************************************************************************
 * Common prototypes
 ******************************************************************************/
function get_abs_pos(orient, ori, obj)
{
	absPos = new THREE.Vector3(0, 0.1, 0);
	absPos.x = ori.x - (obj.x * Math.sin(-orient)) + (obj.z * Math.cos(-orient));
	absPos.z = ori.z + (obj.x * Math.cos(-orient)) + (obj.z * Math.sin(-orient));
	return absPos;
}


/*******************************************************************************
 * ExplodeModifier source
 ******************************************************************************/
/**
 * Make all faces use unique vertices
 * so that each face can be separated from others
 */
THREE.ExplodeModifier = function() {};
THREE.ExplodeModifier.prototype.modify = function(geometry) { geometry.faceVertexUvs[0] = [];};

