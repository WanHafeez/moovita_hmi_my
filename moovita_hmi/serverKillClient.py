#!/usr/bin/env python
# -*- coding: utf-8 -*-

#==============================================================================
#title           :serverKillClient.py
#description     :This file kill client side for Moovita Health Monitoring System.
#author          :[Chia Fu Keong <fukeong@moovita.com>]
#copyright       :Copyright 2016, Moovita
#date            :20171123
#version         :0.1
#status          :Development
#usage           :python serverKillClient.py
#version_notes   :First version
#python_version  :2.7.x
#==============================================================================

'''
 * Copyright (C) Moovita - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Chia Fukeong <fukeong@moovita.com>, November 2017
 *
'''

#**********************************************************************/
#***   Includes                                                     ***/
#**********************************************************************/
import argparse
import const
import psutil


#**********************************************************************/
#***   Global variables                                             ***/
#**********************************************************************/
FLAGS = None


#**********************************************************************/
#***   Main                                                         ***/
#**********************************************************************/
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', type=str, default="", help = 'Server id')
    FLAGS, unparsed = parser.parse_known_args()
    
    ''' Kill serverInitClient based on id '''
    for proc in psutil.process_iter():
        # check whether the process name matches
        if proc.cmdline() == ['python', 'serverInitClient.py', '-s', FLAGS.s]:
            print const.PCOLOR_C.WARNING + "[INFO]" + const.PCOLOR_C.ENDC + " Killed " + proc.name() + " serverInitClient.py -s" + FLAGS.s
            proc.terminate()

    ''' Kill modules just in case it is not killed '''
    for proc in psutil.process_iter():
        if proc.name() in const.SERVER_MODULES[int(FLAGS.s)]:
            print const.PCOLOR_C.WARNING + "[INFO]" + const.PCOLOR_C.ENDC + " Killed " + proc.name()
            proc.kill()
