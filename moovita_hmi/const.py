'''
 * Copyright (C) Moovita - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Chia Fukeong <fukeong@moovita.com>, November 2017
 *
'''

#**********************************************************************/
#***   Classes                                                      ***/
#**********************************************************************/
class PCOLOR_C:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


#**********************************************************************/
#***   Bus stop variables                                           ***/
#***   bus stop = [x,y]
#**********************************************************************/

MAGIC_STOP = [0,0]
FUTURISE_STOP = [1,1]
CONFIG_DEF_PLAN_STOP = [MAGIC_STOP , FUTURISE_STOP]


# buggy [-8.4427, -0.0871],
# [98.9735011497, -35.2953242132],  #buggy [-171.5736, 107.8675],
# [-2.7592, 0.6236],
# [-69.256123, 259.087455],
# [303.875667287, 4.018297066995],
# [-6.926, -225.7616]]

#**********************************************************************/
#***   Health monitoring variables                                  ***/
#**********************************************************************/
'''
SERVER_IP = ['127.0.0.1', '192.168.1.201']
SERVER_UID = ['moovita', 'moovita']
SERVER_PW = ['passw0rd', 'passw0rd']
SERVER_MODULES = [['mooDecisionMaker', 'mooMissionPlanner', 'mooEyeObjDetector'],
                ['localization']]
SERVER_MODULES_DIR = [['/home/fukeong/Desktop/av/dds/moovita_av/modules/decisionMaker/build/', 
                '/home/fukeong/Desktop/av/dds/moovita_av/modules/missionPlanner/build/',
                '/home/fukeong/Desktop/av/dds/moovita_av/modules/mooEyeObjDetector/build/'], 
                '/home/fukeong/Desktop/av/dds/moovita_av/modules/localization']
SERVER_HM_DIR = ['/home/fukeong/Desktop/av/dds/moovita_av/util/moovita_hm/', 
                '/home/moovita/Desktop/moovita_hm']
'''

'''
SERVER_IP = ['127.0.0.1']
SERVER_UID = ['razi']
SERVER_PW = ['pw']
SERVER_MODULES = [['mooDecisionMaker', 'mooMissionPlanner', 'mooPclObjRecognitor']]
SERVER_MODULES_DIR = [['/home/razi/Desktop/moovita_av/modules/decisionMaker/build/', 
                '/home/razi/Desktop/moovita_av/modules/missionPlanner/build/',
                '/home/razi/Desktop/moovita_av/modules/pclObjRecognitor/build/']]
SERVER_HM_DIR = ['/home/razi/Desktop/moovita_av/util/moovita_hmi/']
MODULE_PARKING = "mooParkingLinesDetector"
MODULE_PARKING_DIR = "/home/razi/Desktop/moovita_av_parking/modules/parkingLinesDetector/build/"
MODULE_HEARTBEAT_TIMEOUT = 2.0
'''

SERVER_IP = ['127.0.0.1','10.10.3.42']
SERVER_UID = ['razi','moovita']
SERVER_PW = ['pw','pw']
SERVER_MODULES = [['mooDecisionMaker', 'mooMissionPlanner', 'mooPclObjRecognitor']]
SERVER_MODULES_DIR = [['/home/vi/TheVault/moovita_av_latest/moovita_av_release_080119/modules/decisionMaker/build/', 
                '/home/vi/TheVault/moovita_av_latest/moovita_av_release_080119/modules/missionPlanner/build/',
                '/home/vi/TheVault/moovita_av_latest/moovita_av_release_080119/modules/pclObjRecognitor/build/']]
SERVER_HM_DIR = ['/home/vi/TheVault/moovita_av_latest/moovita_av_release_080119/util/moovita_hmi/']
MODULE_PARKING = "mooParkingLinesDetector"
MODULE_PARKING_DIR = "/home/vi/TheVault/moovita_av_latest/moovita_av_release_080119/modules/parkingLinesDetector/build/"

MODULE_REMOTE = "drive_force_interface.py"
MODULE_REMOTE_DIR = "/home/vi/catkin_ws/src/teleop/modules/"

MODULE_HEARTBEAT_TIMEOUT = 2.0