#!/usr/bin/env python
# -*- coding: utf-8 -*-

#==============================================================================
#title           :serverInit.py
#description     :This file runs WebSocket server for Moovita Human Machine Interface
#author          :[Balaji Ravichandiran <balaji@moovita.com>, Chia Fu Keong <fukeong@moovita.com>]
#copyright       :Copyright 2016, Moovita
#date            :20170321
#version         :0.1
#status          :Development
#usage           :python serverInit.py | python serverInit.py --port=9052
#version_notes   :Load error Handler and Send Status msg 2 when Index file not found
#python_version  :2.7.x
#==============================================================================

'''
 * Copyright (C) Moovita - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Balaji Ravichandiran <balaji@moovita.com> and Chia Fukeong <fukeong@moovita.com>, November 2016
 *
'''

###############################################################
# Imports
###############################################################
import const
import datetime
import math
import logging
import os, os.path
import paramiko
import psutil
import random
import signal
import shlex, subprocess
import string
import sys
import time
import json
from threading import Thread

# Import dds topic for missionPlanning
FILE_PATH = os.path.dirname(os.path.realpath(__file__)) + "/dds/"
sys.path.append(FILE_PATH);
import rticonnextdds_connector as rti

# Import Tornado / Redis Sever
import tornado.web
import tornado.websocket
import tornado.options
import tornado.ioloop
import tornado.httpserver
from tornado import gen
from tornado.options import define, options
from tornado.escape import json_encode
import tornadoredis, redis
define("port", default="9052", help="Port for running WebSocket Server. Eg: python serverInit.py --port=8080")

# Import custom script
import serverInitClient as SIC
status = 0

###############################################################
# Global Variables
###############################################################
DEBUG = True
FILE_LOC = os.path.dirname(os.path.abspath(__file__))
MODULE_TIMEOUT = 5
REDIS_CON = ''
REDIS_HOST = '127.0.0.1'
RTI_CON = ''
SERVER_STATUS = [0] * len(const.SERVER_IP)
SERVER_STATUS_UPDATE = [time.time()] * len(const.SERVER_IP)
SUB_CHANNEL = ["mooVisualizer", "mooHmClient", "pod_status", "pod_monitor"]
PUB_CHANNEL = 'mooHmServer'
LOG_FILENAME = 'logs/'+str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'.log'
#logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG, format='%(asctime)s %(message)s')

###############################################################
# Common Prototypes
###############################################################
@tornado.gen.engine
def check_server():
    while True:
        yield gen.sleep(2)

def current_milli_time():
    '''Get current timestamp in millisecond'''
    return int(round(time.time() * 1000))


def warn(msg):
    '''Prints error message in Red color'''
    print "\033[1;31m " + str(msg) + "\033[0m\n"


def alert(msg):
    '''Prints alerts in Green'''
    print "\033[1;33m " + str(msg) + "\033[0m\n"


def redis_start():
    """Start Redis server and return connection id"""
    _dbcon = redis.StrictRedis(host=REDIS_HOST, port=6379, db=0)
    try:
        _dbcon.ping()
        print "Redis Running"
    except:
        print "Redis Starting"
        os.system('redis-server > redis_log.txt &')
        time.sleep(1)
        try:
            print _dbcon.ping()
            print "Redis Started"
        except:
            print "Redis Can't start'"
            sys.exit(0)
    return _dbcon


###############################################################
# Prototypes
###############################################################

# To publish message to missionPlanner
class ddsHandler(object):  # noqa pylint: disable=R0902
    '''AutoHandler . Client, server communication handeled here.'''

    def _vector2(self, x,y):
        return dict({'x':x, 'y':y})

    @tornado.gen.engine
    def send_default_path(self, ind):
        # {-1, -1} or {0, 0} mean plan from current vehicle pos 
        Reqpath = {'startPos':{'x': -1, 'y': -1}, 'stopPos':[]}
        Reqpath['stopPos'].append(self._vector2(const.CONFIG_DEF_PLAN_STOP[ind][0], 
            const.CONFIG_DEF_PLAN_STOP[ind][1]))
        print Reqpath
        self.ReqGlobalPathOutput.instance.setDictionary(Reqpath)
        self.ReqGlobalPathOutput.write()
        
        # Record next starting point ind
        self.prevInd = ind    

        # Clear previous instance
        self.ReqGlobalPathOutput.clear_members()
        yield gen.sleep(0.1)

    @tornado.gen.engine
    def send_is_pakring(self, b):
        _data = {'b': b}
        self._topic_isParkingEnable_t.instance.setDictionary(_data)
        self._topic_isParkingEnable_t.write()
        
        # Clear previous instance
        self._topic_isParkingEnable_t.clear_members()
        yield gen.sleep(0.1)
    
    @tornado.gen.engine
    def remote_handler(self, remote):
        _data = {'remote_b' : remote}
        self._topic_isRemoteEnable_t.instance.setDictionary(_data)
        self._topic_isRemoteEnable_t.write()

        self._topic_isRemoteEnable_t.clear_members()
        yield gen.sleep(0.1)

    @tornado.gen.engine
    def heartbeat_handler(self):
        print "HeartBeat Handler"
        input = self.heartbeatConnector.getInput("HeartBeatMonitor_Subscriber::HeartBeatMonitorReader")
        while True:
            input.take();
            numOfSamples = input.samples.getLength();
            
            for mod in self.heartbeat:
                if (self.heartbeat[mod]['isInit']) and (time.time() - self.heartbeat[mod]['ts'] > const.MODULE_HEARTBEAT_TIMEOUT):
                    if mod == "POR":
                        for proc in psutil.process_iter():
                            if proc.name() == 'mooPclObjRecognitor':
                                self.heartbeat[mod]['isInit'] = False
                                print const.PCOLOR_C.WARNING + "[INFO]" + const.PCOLOR_C.ENDC + " Killed " + proc.name()
                                proc.kill()
                        SIC.start_process(const.SERVER_MODULES[0][2], const.SERVER_MODULES_DIR[0][2])
            
            for j in range (1, numOfSamples + 1):
                if input.infos.isValid(j):
                    sample = input.samples.getDictionary(j) 
                    self.heartbeat[sample['moduleName']]['ts'] = time.time()
                    self.heartbeat[sample['moduleName']]['isInit'] = True
                    
            #print self.heartbeat
            yield gen.sleep(0.1)

    def __init__(self, *args, **kwargs):
        """Initalize self variables"""
        global FILE_PATH
        self.heartbeat = {'POR': {"isInit" : False, "ts" : 0}}
        self.connector = rti.Connector("DDSROS_ParticipantLibrary::DDSROS", FILE_PATH + "/topicGlobalPathRequest.xml");
        self.heartbeatConnector = rti.Connector("DDSHeartBeat_ParticipantLibrary::DDSHeartBeat", FILE_PATH + "/topicHeartBeat.xml");
        self.ReqGlobalPathOutput  = self.connector.getOutput("RequestGlobalPathPublisher::RequestGlobalPathWriter")
        self._topic_isParkingEnable_t  = self.connector.getOutput("isParkingEnablePublisher::isParkingEnableWriter")
        self._topic_isRemoteEnable_t = self.connector.getOutput("isRemoteEnablePublisher::isRemoteEnableWriter")

        self.prevInd = 1
        self.heartbeat_handler()
        super(ddsHandler, self).__init__(*args, **kwargs)


# To check server on webpage
class IndexHandler(tornado.web.RequestHandler):
    '''Tornado HTTP web handler for get request.'''
    def get(self):
        #print self.request.host
        self.render("hmi.html", hostip=self.request.host)

    def set_extra_headers(self, path):
        self.set_header("Cache-control", "no-cache")

    '''Save Form Data'''
    def post(self):
        data = tornado.escape.json_decode(self.request.body)
        print data


# Render visualizer in iframe
class VisualizerHandler(tornado.web.RequestHandler):
    '''Tornado HTTP web handler for get request.'''
    def get(self):
        #print self.request.host
        self.render("visualizer.html", hostip=self.request.host)

    def set_extra_headers(self, path):
        self.set_header("Cache-control", "no-cache")

    '''Save Form Data'''
    def post(self):
        data = tornado.escape.json_decode(self.request.body)
        print data


# Websocket communication for own client and
# trigger server - server communication
class WebSocketHandler(tornado.websocket.WebSocketHandler, IndexHandler):  # noqa pylint: disable=R0902
    '''
    Websocket Event Handler. It handles request and response between HTML5 and Server
    '''
    global START_STATUS
    client = list()

    def __init__(self, *args, **kwargs):
        '''Initalize self variables'''
        self.dvel = 0
        super(WebSocketHandler, self).__init__(*args, **kwargs)
        self.isConnected = False
        self.isParked = False
        self.isStarted = False
        self.isRemoted = False
        self.procid = 0

    def check_origin(self, origin):  # Websocket cross server communication
        '''Websocket check origin override for cross server communication'''
        return True
    

    @tornado.gen.engine
    def hm_start(self, mode):
        if mode == "ALL":
            self.isStarted = True
            # Start all serverInitClient.py for every servers
            for i in range(0, len(const.SERVER_IP)):
                ''' Run serverInitClient.py for all servers ''' 
                if const.SERVER_IP[i] == '127.0.0.1':
                    self.hm_start_server(i)
                else:
                    self.hm_start_client_server(i)
            
            # Loop to check server / module alive
            while self.isStarted:
                for i in range(0, len(const.SERVER_IP)):
                    
                    ''' If not yet started, no need check time out '''
                    if SERVER_STATUS_UPDATE[i] == 0:
                        continue
                    
                    if const.SERVER_IP[i] == '127.0.0.1':
                        ''' Restart software if died more than 5 secs '''
                        _printMsg = ''
                        if SERVER_STATUS[i] == 1:
                            _printMsg = const.PCOLOR_C.OKGREEN + "[" + const.SERVER_IP[i] + "]:[" + str(SERVER_STATUS[i]) + "]" + const.PCOLOR_C.ENDC
                        else:
                            _printMsg = const.PCOLOR_C.WARNING + "[" + const.SERVER_IP[i] + "]:[" + str(SERVER_STATUS[i]) + "]" + const.PCOLOR_C.ENDC
                        print _printMsg + " SEC:" +  str(time.time() - SERVER_STATUS_UPDATE[i])
                        
                        if SERVER_STATUS[i] == 1 and time.time() - SERVER_STATUS_UPDATE[i] > MODULE_TIMEOUT:
                            SERVER_STATUS[i] = 0
                            self.hm_start_server(i)
                    else:
                    
                        ''' Ping server '''
                        response = os.system("ping -c 1 -w1 " + const.SERVER_IP[i] + " > logs/ping.log")

                        ''' Check respose '''
                        if response == 0:
                            ''' If server is up, check time up '''
                            if SERVER_STATUS[i] == 1 and time.time() - SERVER_STATUS_UPDATE[i] > MODULE_TIMEOUT:
                                SERVER_STATUS[i] = 0
                                self.hm_start_client_server(i)
                        else:
                            SERVER_STATUS[i] = 0
                    
                    ''' Send message due to time out '''
                    if SERVER_STATUS[i] == 0:
                        msg = 's' + str(i + 1) + '[BRK]0,' + str(',').join(['0'] * len(const.SERVER_MODULES[i]))
                        _data = {"type": "mooVisualizer", "data": msg}
                        self.write_message(json.dumps(_data))
                        #self.write_message(msg)
                            
                # Sleep nsec for every ping loop
                yield gen.sleep(5)
            
            # Stop when loop break, sync visualizer with other connectors
            for i in range(0, len(const.SERVER_IP)):
                msg = 's' + str(i + 1) + '[BRK]0,' + str(',').join(['0'] * len(const.SERVER_MODULES[i]))
                _data = {"type": "mooVisualizer", "data": msg}
                self.write_message(json.dumps(_data))
                #self.write_message(msg)
        
        elif mode == "PARK":
            self.isParked = True
            SIC.start_process( const.MODULE_PARKING, const.MODULE_PARKING_DIR )

            # Loop to check server / module alive
            while self.isParked:
                # Send parking message 
                RTI_CON.send_is_pakring(True)

                # Sleep nsec for every ping loop
                yield gen.sleep(1)


        elif mode == "REMOTE":
            self.isRemoted = True
            print "now waiting for remote"
            # SIC.start_process( const.MODULE_REMOTE, const.MODULE_REMOTE_DIR)
            # status = subprocess.call("top" + " -h", shell=True)
            # print status

            cmdA = "gnome-terminal -x bash -c '"
            cmdB = "export ROS_HOSTNAME=localhost"

            cmdC = "export ROS_MASTER_URI=http://localhost:11311"
            cmdD = "echo - && echo - && echo - && "
            cmd1 = "rosrun turtlesim turtlesim_node'"

            command = cmdA + cmdD + cmdB + " && " + cmdC + " && " + cmd1
            # print command
            lala = subprocess.Popen(command, shell=True)
            self.procid = lala.pid
            print self.procid

            # time.sleep(3)
            
            # while self.isRemoted:

            RTI_CON.remote_handler(True)
                # yield gen.sleep(1)
                # subprocess.call("gnome-terminal -x bash -c 'python /home/vi/catkin_ws/src/teleop/modules/drive_force_interface.py'", shell=True)

            

        # elif mode == "s1-loc":
        #     print "now start loc?"

        else:
            msgList = mode.split('-')
            sId = int(msgList[0].replace('s', '')) - 1
            mId = -1
            
            if sId == 0:
                if msgList[1] == 'dm':
                    mId = 0
                elif msgList[1] == 'mp':
                    mId = 1
                elif msgList[1] == 'loc':
                    mId = 2    
                elif msgList[1] == 'mc':
                    mId = 3
                elif msgList[1] == 'lod':
                    mId = 4

            if mId > -1:
                REDIS_CON.publish(PUB_CHANNEL, 'START,' + str(sId) + "," + str(mId))
                print 'START,' + str(sId) + "," + str(mId)
            else:
                print 'id not found, please check msgList to correspond to mId'

    def hm_start_client_server(self, i):
        if not os.system("ping -c 1 -w1 " + const.SERVER_IP[i]):
            ''' Make sure all items are unitialize before start '''
            kill_client_server(i)
            
            ''' SSH to client server and start script '''
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname=const.SERVER_IP[i], username=const.SERVER_UID[i], password=const.SERVER_PW[i])
            
            stdin, stdout, stderr = ssh.exec_command("cd {};nohup python serverInitClient.py -s ".format(const.SERVER_HM_DIR[i]) + str(i) + " > logs/s{}.log &".format(i))
            ssh.close()
            SERVER_STATUS[i] = 1
            print const.PCOLOR_C.OKGREEN + "[INFO]" + const.PCOLOR_C.ENDC + const.SERVER_IP[i] + " is started."
        else:
            print const.PCOLOR_C.WARNING + "[ERROR]" + const.PCOLOR_C.ENDC + const.SERVER_IP[i] + " is not connected."


    @tornado.gen.engine
    def hm_start_server(self, i):
        # Start process
        kill_client_server(i)
        yield gen.sleep(0.5)
        process = subprocess.Popen("nohup python serverInitClient.py -s " + str(i) + " > logs/s{}.log &".format(i), cwd=const.SERVER_HM_DIR[i], shell=True)
        print const.PCOLOR_C.OKGREEN + "[INFO]" + const.PCOLOR_C.ENDC + const.SERVER_IP[i] + ": serverClientInit.py is started."
    

    def hm_stop(self, mode):
        if mode == "ALL":
            self.isStarted = False
            # Kill all serverInitClient.py for every servers
            kill_client_servers();
        
        elif mode == "PARK":
            # Break message loop
            self.isParked = False

            # Kill parking software
            SIC.stop_process("mooParkingLinesDetector")

            # Send last message to parking as False
            RTI_CON.send_is_pakring(False)

        elif mode == "REMOTE":
            self.isRemoted = False
            print "remote now stop"
            for p in psutil.process_iter():
                # print "name:{} | pid:{}" .format(p.name() , p.ppid())
                if p.name() == 'turtlesim_node':
                    p.kill()
                # if p.name() == 'rosmaster':
                #     p.kill()


            # p.kill()
            print self.procid

            # print self.procid

            # os.killpg(os.getpgid(status.pid), signal.SIGTERM)
            
            # SIC.stop_process(const.MODULE_PARKING)

            RTI_CON.remote_handler(False)
        # elif mode == "s1-loc":
        #     print "now stop loc?"
        
        else:
            msgList = mode.split('-')
            sId = int(msgList[0].replace('s', '')) - 1
            mId = -1
            
            if sId == 0:
                if msgList[1] == 'dm':
                    mId = 0
                elif msgList[1] == 'mp':
                    mId = 1
                elif msgList[1] == 'loc':
                    mId = 2    
                elif msgList[1] == 'mc':
                    mId = 3
                elif msgList[1] == 'lod':
                    mId = 4

            if mId > -1:
                REDIS_CON.publish(PUB_CHANNEL, 'STOP,' + str(sId) + "," + str(mId))
                print 'STOP,' + str(sId) + "," + str(mId)
            
            else:
                print 'id not found, please check msgList to correspond to mId'

    @tornado.gen.engine
    def listen_redis(self):
        self.client = tornadoredis.Client(host=REDIS_HOST)
        self.client.connect()
        yield tornado.gen.Task(self.client.subscribe, SUB_CHANNEL)
        self.client.listen(self.redis_message)
        while True:
            #print "sleep"
            yield gen.sleep(5)

    def on_close(self):  # client disconnected
        '''WebSocket close event handler'''
        warn("Client disconnected\n")
        self.redis_close()
        self.isConnected = False
        WebSocketHandler.client.remove(self)
 
    def on_message(self, message):
        '''WebSocket ON Message Handler'''
        if DEBUG:
            print"\n***************\n"
            logging.info("Got message %r", message)
        
        parsed = tornado.escape.json_decode(message)
        if parsed['type'] == "START":
            self.hm_start(parsed['attribute'])
        elif parsed['type'] == "STOP":
            self.hm_stop(parsed['attribute'])
        elif parsed['type'] == "REQ_PATH":
            RTI_CON.send_default_path(int(parsed['attribute']))
            

    def on_shutdown():
        '''Tornado close handler on keyboard interupt'''
        alert('Shutting down')
        tornado.ioloop.IOLoop.instance().stop()


    def open(self, *args):
        '''WebSocket Connection Open Handler'''
        if self not in WebSocketHandler.client:
            alert("New client connected " + str(self.request.host))
            WebSocketHandler.client.append(self)
        self.isConnected = True
        self.stream.set_nodelay(True) #tcp no delay flag
        self.listen_redis()


    def redis_close(self):
        if self.client.subscribed:
            warn("Redis Connection Closed\n")
            self.client.unsubscribe(SUB_CHANNEL)
            self.client.disconnect()
    
    
    def redis_message(self, message):
        '''Handle redis Subscribe message'''
        if message.kind == 'message':
            
            if message.channel == 'mooHmClient':
                msgList = str(message.body).split('[BRK]')
                print const.PCOLOR_C.OKBLUE + "[SERVER" + str(int(msgList[0]) + 1) + "]: " + const.PCOLOR_C.ENDC + "[" + msgList[1] + "]"
                if int(msgList[0]) < len(SERVER_STATUS_UPDATE):
                    SERVER_STATUS[int(msgList[0])] = 1
                    SERVER_STATUS_UPDATE[int(msgList[0])] = time.time()
                    msgList[0] = 's' + str(int(msgList[0]) + 1)
                    msgList[1] = '1,' + msgList[1]
                    msg = str("[BRK]").join(msgList)
                    #self.write_message(msg)
                    _data = {"type": "mooVisualizer", "data": msg}
                    self.write_message(json.dumps(_data))
            elif message.channel == 'mooVisualizer':
                _data = {"type": "mooVisualizer", "data": message.body}
                self.write_message(json.dumps(_data))
            else:
                ''' Forward message to websocket '''
                self.write_message(str(message.body))

        elif message.kind == 'disconnect':
            # Do not try to reconnect, just send a message back
            # to the client and close the client connection
            print ('The connection terminated '
                               'due to a Redis server error.')
            self.redis_close()

def init_service():
        # Define tornada server assets
        application = tornado.web.Application([
                (r"/css/(.*)", tornado.web.StaticFileHandler, {"path": (FILE_LOC+"/css")},),
                (r"/logs/(.*)", tornado.web.StaticFileHandler, {"path": FILE_LOC+"/logs"},),
                (r"/fonts/(.*)", tornado.web.StaticFileHandler, {"path": FILE_LOC+"/fonts"},),
                (r"/images/(.*)", tornado.web.StaticFileHandler, {"path": FILE_LOC+"/images"},),
                (r"/js/(.*)", tornado.web.StaticFileHandler, {"path": FILE_LOC+"/js"},),
                (r"/libs/(.*)", tornado.web.StaticFileHandler, {"path": FILE_LOC+"/libs"},),
                (r"/models/(.*)", tornado.web.StaticFileHandler, {"path": FILE_LOC+"/models"},),
                (r"/curbLines/(.*)", tornado.web.StaticFileHandler, {"path": FILE_LOC+"/curbLines"},),
                (r"/textures/(.*)", tornado.web.StaticFileHandler, {"path": FILE_LOC+"/textures"},),
                (r"/", IndexHandler),
                (r"/hmi.html", IndexHandler),
                (r"/index", IndexHandler),
                (r"/visualizer.html", VisualizerHandler),
                (r"/ws", WebSocketHandler),

        ], max_buffer_size = 5127772160)

        # Initialize tornado servers
        application.listen(options.port)
        APP_IOLOOP = tornado.ioloop.IOLoop.instance()  # tornado IOLOOP assign
        
        ''' Add signal control '''
        for sigs in (signal.SIGABRT, signal.SIGILL, signal.SIGINT, signal.SIGSEGV, signal.SIGTERM):
            signal.signal(sigs,
                  lambda sig,
                  frame: APP_IOLOOP.add_callback_from_signal(on_shutdown, options.port))
        print "Running Server at port num: " + str(options.port)

        # Start server app
        APP_IOLOOP.start()


def kill_client_server(i):
    ''' Reset update timing '''
    SERVER_STATUS_UPDATE[i] = 0
    
    ''' Kill serverInitClient.py for serverId ''' 
    if const.SERVER_IP[i] == '127.0.0.1':
        # Start process
        process = subprocess.Popen("python serverKillClient.py -s " + str(i), cwd=const.SERVER_HM_DIR[i], shell=True)
        print const.PCOLOR_C.WARNING + "[INFO]" + const.PCOLOR_C.ENDC + const.SERVER_IP[i] + ": serverClientInit.py is stopped."
    else:
        if not os.system("ping -c 1 -w1 " + const.SERVER_IP[i]): # responce 0 == connected
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname=const.SERVER_IP[i], username=const.SERVER_UID[i], password=const.SERVER_PW[i])
            
            stdin, stdout, stderr = ssh.exec_command("cd {};python serverKillClient.py".format(const.SERVER_HM_DIR[i]))
            ssh.close()
            print const.PCOLOR_C.WARNING + "[INFO]" + const.PCOLOR_C.ENDC + const.SERVER_IP[i] + ": serverClientInit.py is stopped."
        else:
            print const.PCOLOR_C.WARNING + "[ERROR]" + const.PCOLOR_C.ENDC + const.SERVER_IP[i] + " is not connected."


def kill_client_servers():
    ''' Kill all servers '''
    for i in range(0, len(const.SERVER_IP)):
        kill_client_server(i)


def on_shutdown(portNum):
    kill_client_servers()
    alert('Shutting down port num: ' + str(portNum))
    tornado.ioloop.IOLoop.instance().stop()

###############################################################
# Main
###############################################################
if __name__ == "__main__":
    options.parse_command_line()
    REDIS_CON = redis_start() 
    try:
        RTI_CON = ddsHandler()  # Init dds publisher for missionPlanner   
        init_service()   # Init tonardo service
    except:
        logging.exception('Got exception in Main Handler')
        raise

