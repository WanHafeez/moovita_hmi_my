#!/usr/bin/env python

import time
import rospy
import tf
import sys
import math
import numpy as np
from transforms3d.euler import quat2euler

from nav_msgs.msg import Path, Odometry
from sensor_msgs.msg import Imu
import sensor_msgs.point_cloud2 as pc2
from geometry_msgs.msg import PoseStamped, Quaternion
from std_msgs.msg import String
from moovita_message_definitions.msg import wheel_console
from moovita_message_definitions.msg import pod_to_acs
from moovita_message_definitions.msg import acs_to_pod
from moovita_message_definitions.msg import rdm_buttons
from moovita_message_definitions.msg import auto_controller
from datetime import timedelta
import std_msgs.msg
import logging
import os
import redis
import json
from sensor_msgs.msg import PointCloud2, PointField

#---color
PURPLE = '\033[95m'
CYAN = '\033[96m'
DARKCYAN = '\033[36m'
BLUE = '\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'
END = '\033[0m'

# current Millisecond function
def current_milli_time():
    """Get current timestamp in millisecond"""
    return int(round(time.time() * 1000))
    
class MONITOR:

    def redis_connect(self):
        ''' Start Redis server and return connection id '''
        self.redis_con = redis.StrictRedis(host=self.redis_host, port=6379, db=0)
        try:
            self.redis_con.ping()
            print('Redis Running') 
        except Exception as e:
            print(e)
            print('Redis Starting') 
            os.system('redis-server > redis_log.txt &')
            time.sleep(1)
            try:
                self.redis_con.ping()
                print("Redis Started")
            except Exception as e:
                print(e)
                print("Redis Can't start'")
                sys.exit(0)
    
    def pod_handler(self, data):
        self.velocity = data.Velocity
        _ts = current_milli_time()
        self.monitor_data["pod_msg"]["hz"] = _ts - self.monitor_data["pod_msg"]["ts"]
        self.monitor_data["pod_msg"]["ts"] = _ts

        
    def acs_handler(self, data):
        _ts = current_milli_time()
        self.monitor_data["acs_msg"]["hz"] = _ts - self.monitor_data["acs_msg"]["ts"]
        self.monitor_data["acs_msg"]["ts"] = _ts

    def wheel_console_handler(self, data):
        _ts = current_milli_time()
        self.monitor_data["wheel_msg"]["hz"] = _ts - self.monitor_data["wheel_msg"]["ts"]
        self.monitor_data["wheel_msg"]["ts"] = _ts

    def rdm_buttons_handler(self, data):
        #print "button"
        _ts = current_milli_time()
        self.monitor_data["rdm_buttons_msg"]["hz"] = _ts - self.monitor_data["rdm_buttons_msg"]["ts"]
        self.monitor_data["rdm_buttons_msg"]["ts"] = _ts

    def auto_controller_handler(self, data):
        _ts = current_milli_time()
        self.monitor_data["auto_controller_msg"]["hz"] = _ts - self.monitor_data["auto_controller_msg"]["ts"]
        self.monitor_data["auto_controller_msg"]["ts"] = _ts

    def top_lidar_handler(self, data):
        _ts = current_milli_time()
        self.monitor_data["top_lidar_msg"]["hz"] = _ts - self.monitor_data["top_lidar_msg"]["ts"]
        self.monitor_data["top_lidar_msg"]["ts"] = _ts

    def fused_lidar_handler(self, data):
        _ts = current_milli_time()
        self.monitor_data["fused_lidar_msg"]["hz"] = _ts - self.monitor_data["fused_lidar_msg"]["ts"]
        self.monitor_data["fused_lidar_msg"]["ts"] = _ts

    def bottom_lidar_handler(self, data):
        _ts = current_milli_time()
        self.monitor_data["bottom_lidar_msg"]["hz"] = _ts - self.monitor_data["bottom_lidar_msg"]["ts"]
        self.monitor_data["bottom_lidar_msg"]["ts"] = _ts

    

    def ekf_handler(self, data):
        _ts = current_milli_time()
        self.monitor_data["ekf_msg"]["hz"] = _ts - self.monitor_data["ekf_msg"]["ts"]
        self.monitor_data["ekf_msg"]["ts"] = _ts

    def odom_handler(self, data):
        _ts = current_milli_time() #(float(data.header.stamp.secs) + float(data.header.stamp.nsecs / 1000000000.0)) * 1000
        if _ts != self.monitor_data["odom_msg"]["ts"]:
            self.monitor_data["odom_msg"]["hz"] = int(_ts - self.monitor_data["odom_msg"]["ts"])
            self.monitor_data["odom_msg"]["ts"] = _ts
            self.monitor_data["odom_msg"]["sys_ts"] = current_milli_time()

    def imu_handler(self, data):
        #print "called"
        _ts = (float(data.header.stamp.secs) + float(data.header.stamp.nsecs / 1000000000.0)) * 1000
        if _ts != self.monitor_data["imu_msg"]["ts"]:
            self.monitor_data["imu_msg"]["hz"] = int(_ts - self.monitor_data["imu_msg"]["ts"])
            self.monitor_data["imu_msg"]["ts"] = _ts
            self.monitor_data["imu_msg"]["sys_ts"] = current_milli_time()

    def loc_handler(self, data):
        #print "called"
        _ts = current_milli_time()
        self.monitor_data["loc_msg"]["hz"] = _ts - self.monitor_data["loc_msg"]["ts"]
        self.monitor_data["loc_msg"]["ts"] = _ts
    
    def print_normal(self, msg):
        print GREEN, msg, END
    
    def print_error(self, msg):
        print RED, msg, END
    
    def print_warn(self, msg):
        print YELLOW, msg, END

    def check_callback(self, event):
        #print 'Timer called at ' + str(event.current_real)
        _ts = current_milli_time()
        print "\n******* DELTA TIME **********"
        if _ts - self.monitor_data["pod_msg"]["ts"] < 500:
            self.print_normal('POD MSG : ' + str(self.monitor_data["pod_msg"]["hz"]))
            self.monitor_data["pod_msg"]["status"] = self.monitor_data["pod_msg"]["hz"]
        else:
            self.print_error('POD MSG : NO Data')
            self.monitor_data["pod_msg"]["status"] = "No Data"

        if _ts - self.monitor_data["acs_msg"]["ts"] < 100:
            self.print_normal('ACS MSG : ' + str(self.monitor_data["acs_msg"]["hz"]))
            self.monitor_data["acs_msg"]["status"] = self.monitor_data["acs_msg"]["hz"]
        else:
            self.print_error('ACS MSG : NO Data')
            self.monitor_data["acs_msg"]["status"] = "No Data"

        if _ts - self.monitor_data["imu_msg"]["ts"] < 500:
            print GREEN, 'IMU MSG : ' , self.monitor_data["imu_msg"]["hz"]
            self.monitor_data["imu_msg"]["status"] = self.monitor_data["imu_msg"]["hz"]
        else:
            self.print_error( 'IMU MSG : NO Data')
            self.monitor_data["imu_msg"]["status"] = "No Data"
            
        if _ts - self.monitor_data["loc_msg"]["ts"] < 500:
            print GREEN, ' LOC MSG : ' , self.monitor_data["loc_msg"]["hz"]
            self.monitor_data["loc_msg"]["status"] = self.monitor_data["loc_msg"]["hz"]
        elif abs(self.velocity) < 0.1:
            print YELLOW, 'LOC MSG : ZERO VEL', END
            self.monitor_data["loc_msg"]["status"] = "ZERO VEL"
        else:
            self.print_error( 'LOC MSG : NO Data')
            self.monitor_data["loc_msg"]["status"] = "No Data"

        if _ts - self.monitor_data["odom_msg"]["ts"] < 500:
            print  GREEN, 'ODOM MSG : ' , self.monitor_data["odom_msg"]["hz"]
            self.monitor_data["odom_msg"]["status"] = self.monitor_data["odom_msg"]["hz"]
        else:
            self.print_error( 'ODOM MSG : NO Data')
            self.monitor_data["odom_msg"]["status"] = "No Data"

        if _ts - self.monitor_data["ekf_msg"]["ts"] < 500:
            print GREEN,'EKF MSG : ' , self.monitor_data["ekf_msg"]["hz"]
            self.monitor_data["ekf_msg"]["status"] = self.monitor_data["ekf_msg"]["hz"]
        else:
            self.print_error( 'EKF MSG : NO Data')
            self.monitor_data["ekf_msg"]["status"] = "No Data"

        if _ts - self.monitor_data["top_lidar_msg"]["ts"] < 500:
            print GREEN,'TOP LIDAR : ' , self.monitor_data["top_lidar_msg"]["hz"]
            self.monitor_data["top_lidar_msg"]["status"] = self.monitor_data["top_lidar_msg"]["hz"]
        else:
            self.print_error( 'TOP LIDAR : NO Data')
            self.monitor_data["top_lidar_msg"]["status"] = "No Data"

        if _ts - self.monitor_data["bottom_lidar_msg"]["ts"] < 500:
            print GREEN,'BOTTOM LIDAR : ' , self.monitor_data["bottom_lidar_msg"]["hz"]
            self.monitor_data["bottom_lidar_msg"]["status"] = self.monitor_data["bottom_lidar_msg"]["hz"]
        else:
            self.print_error( 'BOTTOM LIDAR : NO Data')
            self.monitor_data["bottom_lidar_msg"]["status"] = "No Data"

        if _ts - self.monitor_data["fused_lidar_msg"]["ts"] < 500:
            print GREEN,'FUSED LIDAR : ' , self.monitor_data["fused_lidar_msg"]["hz"]
            self.monitor_data["fused_lidar_msg"]["status"] = self.monitor_data["fused_lidar_msg"]["hz"]
        else:
            self.print_error( 'FUSED LIDAR : NO Data')
            self.monitor_data["fused_lidar_msg"]["status"] = "No Data"

        if _ts - self.monitor_data["wheel_msg"]["ts"] < 500:
            print GREEN,'WheelConsole : ' , self.monitor_data["wheel_msg"]["hz"]
            self.monitor_data["wheel_msg"]["status"] = self.monitor_data["wheel_msg"]["hz"]
        else:
            self.print_error( 'WheelConsole : NO Data')
            self.monitor_data["wheel_msg"]["status"] = "No Data"

        if _ts - self.monitor_data["rdm_buttons_msg"]["ts"] < 500:
            print GREEN,'RDM BUTTONS : ' , self.monitor_data["rdm_buttons_msg"]["hz"]
            self.monitor_data["rdm_buttons_msg"]["status"] = self.monitor_data["rdm_buttons_msg"]["hz"]
        else:
            self.print_error( 'RDM BUTTONS : NO Data')
            self.monitor_data["rdm_buttons_msg"]["status"] = "No Data"

        if _ts - self.monitor_data["auto_controller_msg"]["ts"] < 500:
            print GREEN,'AUTO CONTROLLER : ' , self.monitor_data["auto_controller_msg"]["hz"]
            self.monitor_data["auto_controller_msg"]["status"] = self.monitor_data["auto_controller_msg"]["hz"]
        else:
            self.print_error( 'AUTO CONTROLLER : NO Data')
            self.monitor_data["auto_controller_msg"]["status"] = "No Data"
            
        _data = {"type": "pod_monitor", "data" : self.monitor_data}
        self.redis_con.publish("pod_monitor", json.dumps(_data))
        
    def __init__(self, host_name='localhost'):
        self.redis_host = host_name
        self.redis_connect()  # redis server connection
        self.monitor_data = {
                            "pod_msg":{"ts":-1, "hz":-1, "status": "No Data"}, 
                            "acs_msg":{"ts":-1, "hz":-1, "status": "No Data"},
                            "wheel_msg":{"ts":-1, "hz":-1, "status": "No Data"},
                            "rdm_buttons_msg":{"ts":-1, "hz":-1, "status": "No Data"},
                            "auto_controller_msg":{"ts":-1, "hz":-1, "status": "No Data"},
                            "imu_msg":{"ts":-1, "hz":-1, "sys_ts": -1, "status": "No Data"},
                            "odom_msg":{"ts":-1, "hz":-1, "sys_ts": -1, "status": "No Data"},
                            "loc_msg":{"ts":-1, "hz":-1, "status": "No Data"},
                            "ekf_msg":{"ts":-1, "hz":-1, "status": "No Data"},
                            "bottom_lidar_msg":{"ts":-1, "hz":-1, "status": "No Data"},
                            "top_lidar_msg":{"ts":-1, "hz":-1, "status": "No Data"},
                            "fused_lidar_msg":{"ts":-1, "hz":-1, "status": "No Data"},
                            }

        
        self.velocity = 0
        
        # Create a Subscriber
        rospy.Subscriber("/pod_to_acs", pod_to_acs, self.pod_handler)
        rospy.Subscriber("/acs_to_pod", acs_to_pod, self.acs_handler)
        
        rospy.Subscriber("/wheel_console", wheel_console, self.wheel_console_handler)
        rospy.Subscriber("/rdm_buttons", rdm_buttons, self.rdm_buttons_handler)
        rospy.Subscriber("/auto_controller", auto_controller, self.auto_controller_handler)
        
        rospy.Subscriber('/an_device/Imu', Imu, self.imu_handler)
        rospy.Subscriber('/odom_localization', Odometry, self.loc_handler)
        rospy.Subscriber('/imuodom', Odometry, self.odom_handler)
        rospy.Subscriber('/ekf_localization', Odometry, self.ekf_handler)
        rospy.Subscriber('/ns2/rslidar_points', PointCloud2, self.top_lidar_handler)
        rospy.Subscriber('/ns1/rslidar_points', PointCloud2, self.bottom_lidar_handler)
        rospy.Subscriber('/fused/rslidar_points', PointCloud2, self.fused_lidar_handler)

        

        rospy.Timer(rospy.Duration(0.1), self.check_callback)
 


if __name__ == '__main__':
    try:
        rospy.init_node("module_monitor")
        MONITOR()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
