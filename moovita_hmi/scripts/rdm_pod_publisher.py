#!/usr/bin/env python

""" RDM POD Publisher 
# -*- coding: utf-8 -*-
# 
# Author : Balaji Ravichandiran
# Email : balaji@moovita.com
# Department : Moovita, SG
# Last modified: 2-Jul-2018
# version comments - clean up code
# Description : ACS Interface
"""
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import TwistStamped,Pose,Twist, Vector3
from sensor_msgs.msg import PointCloud2, PointField
import redis, math, time, os
from moovita_message_definitions.msg import wheel_console
from moovita_message_definitions.msg import pod_to_acs
from moovita_message_definitions.msg import acs_to_pod
from moovita_message_definitions.msg import rdm_buttons
from moovita_message_definitions.msg import auto_controller
from datetime import timedelta
import std_msgs.msg
from ConfigParser import SafeConfigParser
import tornado.web
import tornado.websocket
import tornado.options
import tornado.ioloop
from tornado import gen
import signal
import json

FILE_LOCATION = os.path.dirname(os.path.abspath(__file__))

#------------Config Parser---------
config = SafeConfigParser()
config.read(FILE_LOCATION+'/config/config.ini')

WHEEL_RADIUS = config.getfloat('RDM_POD', 'WHEEL_RADIUS')
NUM_TICK_WHEEL_REV = config.getfloat('RDM_POD', 'NUM_TICK_WHEEL_REV')


class RdmPodToAcsPublisher(object):
    '''Class to read physical button state for Lights and mode switch control'''

    def redis_connect(self):
        ''' Start Redis server and return connection id '''
        self.redis_con = redis.StrictRedis(host=self.redis_host, port=6379, db=0)
        try:
            self.redis_con.ping()
            print('Redis Running') 
        except Exception as e:
            os.system('redis-server > redis_log.txt &')
            time.sleep(1)
            try:
                self.redis_con.ping()
                print("Redis Started")
            except Exception as e:
                print(e)
                sys.exit(0)

    
    def process_buttonState(self, data):        
        ''' Subscribe Button State Topic '''
        self.pod_buttons['ts'] = data.header.stamp.to_sec()
        self.pod_buttons['isAuto'] = data.is_auto
        self.pod_buttons['FlashLeftDi'] = data.FlashLeft
        self.pod_buttons['FlashRightDi'] = data.FlashRight
        self.pod_buttons['HeadlampsOn'] = data.HeadlampsOn
        self.pod_buttons['InteriorLightsOn'] = data.InteriorLightsOn
        
        _data = {"type": "pod_buttons", "data" : self.pod_buttons}
        self.redis_con.publish("pod_buttons", json.dumps(_data))

    
    def joystick_callback(self, data):        
        ''' Subscribe Wheel Console State Topic '''
        self.joystick_commands['seq'] = data.header.seq
        self.joystick_commands['ts'] = data.header.stamp.to_sec()
        self.joystick_commands['is_init'] = data.is_init
        self.joystick_commands['cmd_steering'] = data.cmd_steering
        self.joystick_commands['cmd_speed'] = data.cmd_speed
        self.joystick_commands['is_brake'] = data.is_brake
        self.joystick_commands['is_estop'] = data.is_estop


    def auto_command_callback(self, data):        
        ''' Subscribe Auto controller Topic '''
        self.auto_commands['seq'] = data.header.seq
        self.auto_commands['ts'] = data.header.stamp.to_sec()
        self.auto_commands['cmd_steering'] = data.cmd_steering
        self.auto_commands['cmd_speed'] = data.cmd_speed
        self.auto_commands['is_brake'] = data.is_brake
        self.auto_commands['is_estop'] = data.is_estop

    
    def pod_callback(self, data):
        ''' Subscribe Pod message '''
        self.pod_status['seq'] = data.header.seq
        
        try:
            podState = self.pod_state_defintion[data.PodState]
        except:
            podState = "UNKNOWN"
        
        self.pod_status['PodState'] = podState
        self.pod_status['Velocity'] = data.Velocity*0.036 #feedback in cm/s
        self.pod_status['velocityms'] = data.Velocity/100.
        self.pod_status['SteeringPercent'] = data.SteeringPercent
        self.pod_status['StateOfCharge'] = data.StateOfCharge
        self.pod_status['VSupply12'] = data.VSupply12
        self.pod_status['VBat48'] = data.VBat48
        self.pod_status['VmsFaultCode'] = data.VmsFaultCode
        self.pod_status['McuFaultCode'] = data.McuFaultCode
        self.pod_status['ParkBrakeState'] = data.ParkBrakeState
        self.pod_status['EStop'] = data.EStopTriggered
        self.pod_status['RfEstopTriggered'] = data.RfEstopTriggered
        self.pod_status['LeftDoorClosed'] = data.LeftDoorClosed
        self.pod_status['RightDoorClosed'] = data.RightDoorClosed

        self.pod_status['McuTemp'] = data.McuTemp/10.
        self.pod_status['MotorTemp'] = data.MotorTemp/10.
        
        alpha = 0.915
        self.pod_status['estimated_acc'] = alpha * self.pod_status['estimated_acc'] + (1 - alpha) * (self.pod_status['velocityms'] - self.pod_status['prev_velocity']) /  0.02
        
        self.pod_status['prev_velocity'] = self.pod_status['velocityms']
        
        self.pod_status['FrontLeftOuterUltrasonic'] = data.FrontLeftOuterUltrasonic
        self.pod_status['FrontLeftCentreUltrasonic'] = data.FrontLeftOuterUltrasonic # data.FrontLeftCentreUltrasonic
        self.pod_status['FrontRightCentreUltrasonic'] = data.FrontLeftCentreUltrasonic #data.FrontRightCentreUltrasonic
        self.pod_status['FrontRightOuterUltrasonic'] = data.FrontRightOuterUltrasonic

        self.pod_status['RearLeftOuterUltrasonic'] = data.RearLeftOuterUltrasonic
        self.pod_status['RearLeftCentreUltrasonic'] = data.RearLeftOuterUltrasonic # data.RearLeftCentreUltrasonic
        self.pod_status['RearRightCentreUltrasonic'] = data.RearLeftCentreUltrasonic #data.RearRightCentreUltrasonic
        self.pod_status['RearRightOuterUltrasonic'] = data.RearRightOuterUltrasonic

        
        if self.pod_buttons['isAuto'] and self.pod_status['LeftDoorClosed'] and self.pod_status['RightDoorClosed']:
            self.pod_status['isAuto'] = 1
            self.redis_con.set("drive_mode", "AUTO", 1 )
        else:
            self.pod_status['isAuto'] = 0
            self.redis_con.set("drive_mode", "Manual", 1 )

        try:
            self.pod_status["global_auto_odom"] = float(self.redis_con.get("auto_odom"))
            self.pod_status["global_manual_odom"] = float(self.redis_con.get("manual_odom"))
        except:
            pass
       
        if self.pod_status["initalized"]:
            left_nticks = data.RearLeftWheelCnt - self.pod_status["leftWHeelCount"]
            right_nticks = data.RearRightWheelCnt - self.pod_status["rightWheelCount"]
            
            if left_nticks < -180:
                    left_nticks += 255
            if left_nticks > 200:
                left_nticks = 255 - left_nticks
            
            ld = abs(left_nticks/NUM_TICK_WHEEL_REV * 2 * math.pi * WHEEL_RADIUS)

            if right_nticks < -180:
                right_nticks += 255

            if right_nticks > 200:
                right_nticks = 255 - right_nticks
                
            rd = abs(right_nticks/NUM_TICK_WHEEL_REV * 2 * math.pi * WHEEL_RADIUS)

            d = (ld + rd)/2.0

            if d > 100:
                d = 0
                
            #print d
            self.pod_status["trip_odom"] = self.pod_status["trip_odom"] + d

            if self.pod_status['isAuto'] and self.pod_status["global_auto_odom"] != -1:
                self.pod_status["global_auto_odom"] = self.pod_status["global_auto_odom"]  + d

            if self.pod_status["global_manual_odom"] != -1:
                self.pod_status["global_manual_odom"] = self.pod_status["global_manual_odom"]  + d

            self.redis_con.set("auto_odom", str(self.pod_status['global_auto_odom']) )
            self.redis_con.set("manual_odom", str(self.pod_status['global_manual_odom']))
            self.redis_con.set("trip_odom", str(self.pod_status['trip_odom']))
            
        self.pod_status["leftWHeelCount"] = data.RearLeftWheelCnt
        self.pod_status["rightWheelCount"] = data.RearRightWheelCnt
        self.pod_status["initalized"] = True
        self.pod_status["joystick_init"] = self.joystick_commands['is_init']

        self.redis_con.set("speed", str(self.pod_status['Velocity']), 1 )
        self.redis_con.set("stpos", str(self.pod_status['SteeringPercent']), 1 )

        _data = {"type": "pod_status", "data" : self.pod_status}
        self.redis_con.publish("pod_status", json.dumps(_data))

    @tornado.gen.engine
    def send_acs_to_pod_command(self):
        ''' Send ACS to POD message'''
        print "Send ACS Command"
        
        while True:

            if self.pod_status['isAuto']:
                cmdSteering = self.auto_commands['cmd_steering']
                cmdVelocity = int(self.auto_commands['cmd_speed']*10.)
            else:
                cmdSteering = self.joystick_commands['cmd_steering']
                cmdVelocity = int(self.joystick_commands['cmd_speed']*10.)
            
                
        
            if self.joystick_commands['is_brake'] == True or \
                abs(self.joystick_commands['cmd_speed']) == 0 or \
                self.pod_status['MotorTemp'] > 90:
                cmdVelocity = 0


            if self.joystick_commands['is_estop'] == True or self.joystick_commands['is_estop'] == True or self.auto_commands['cmd_speed'] < -10 or self.joystick_commands['is_init'] == False:
                isParkBrakeOff = False
            else:
                isParkBrakeOff = True

            ## Timeout check
            ts = rospy.Time.now()
            if (((ts.to_sec() - self.joystick_commands['ts']) > 1) and (self.pod_status['isAuto'] == False)) or \
                (((ts.to_sec() - self.auto_commands['ts']) > 1) and (self.pod_status['isAuto'])) :
                print "Timeout", ts.to_sec(), self.pod_status['isAuto']
                cmdVelocity = 0
            
            #k_compensation = 25
            #cmdVelocity = int(((cmdVelocity*36.) - k_compensation * self.pod_status['estimated_acc'])/36.)
            #print cmdVelocity
            
            if cmdVelocity < 0:
                throttlePer = -100 #reverse
            else:
                throttlePer = 100

            self.pod_status['ReqSteering'] = cmdVelocity/10.
            self.pod_status['ReqSpeed'] = cmdSteering
            
            if self.pod_status['isAuto'] and abs(cmdVelocity) > 5:
                self.pod_buttons['RightDoorDisable'] = 1
                self.pod_buttons['LeftDoorDisable'] = 1
                self.pod_buttons['LeftDoorSwitchIllum'] = 1
                self.pod_buttons['RightDoorSwitchIllum'] = 1
            else:
                self.pod_buttons['RightDoorDisable'] = 0
                self.pod_buttons['LeftDoorDisable'] = 0
                self.pod_buttons['LeftDoorSwitchIllum'] = 2
                self.pod_buttons['RightDoorSwitchIllum'] = 2
            
            msg = acs_to_pod()
            msg.header.stamp = rospy.Time.now() # Note you need to call rospy.init_node() before this will work
            msg.header.frame_id = 'moovita'

            msg.StateRequest = 11 #acs mode
            msg.Throttle = throttlePer
            msg.Velocity = abs(cmdVelocity)
            msg.SteeringPercent = cmdSteering
            msg.ParkBrakeOff =  isParkBrakeOff
            
            msg.LightingOverride = self.pod_buttons['LightingOverride']
            msg.HeadlampsOn = self.pod_buttons['HeadlampsOn']
            msg.InteriorLightsOn = self.pod_buttons['InteriorLightsOn']
            msg.FlashLeftDi = self.pod_buttons['FlashLeftDi']
            msg.FlashRightDi = self.pod_buttons['FlashRightDi']

            msg.LeftDoorDisable = self.pod_buttons['LeftDoorDisable']
            msg.RightDoorDisable = self.pod_buttons['RightDoorDisable']
            msg.LeftDoorSwitchIllum = self.pod_buttons['LeftDoorSwitchIllum']
            msg.RightDoorSwitchIllum = self.pod_buttons['RightDoorSwitchIllum']
            self.cmd_pub.publish(msg)

            
            #print int(time.time()*1000) - self.pod_status["ts"]
            self.pod_status["ts"] = int(time.time()*1000)
            
            yield gen.sleep(0.005)


    def __init__(self, *args, **kwargs):
        ''' Initalize class variables '''
        self.redis_host = "localhost"
        self.redis_connect()  # redis server connection
        rospy.init_node("pod_to_acs_publisher")
        rospy.Subscriber("/pod_to_acs", pod_to_acs, self.pod_callback)
        rospy.Subscriber("/wheel_console", wheel_console, self.joystick_callback)
        rospy.Subscriber("/auto_controller", auto_controller, self.auto_command_callback)
        rospy.Subscriber("/rdm_buttons", rdm_buttons, self.process_buttonState)
        self.cmd_pub = rospy.Publisher('/acs_to_pod', acs_to_pod, queue_size=1)

        self.auto_commands = {"seq" : -1, "ts": -1, "is_init" : 0, "cmd_steering": 0, "cmd_speed" : 0, "is_brake" : 0, "is_estop": 0}
        self.joystick_commands = {"seq" : -1, "ts": -1, "is_init" : 0, "cmd_steering": 0, "cmd_speed" : 0, "is_brake" : 0, "is_estop": 0}
        self.pod_buttons = {"ts": 0, "isBrake":0, "isAuto":0, "FlashLeftDi":0, "FlashRightDi":0, "HeadlampsOn":0, "InteriorLightsOn":0, "LightingOverride":1, "LeftDoorDisable":0, "RightDoorDisable": 0, "LeftDoorSwitchIllum":0, "RightDoorSwitchIllum": 0 }
        self.pod_state_defintion = {0:"UNKNOWN", 2:"MANUAL", 4: "UNKNOWN", 5:"HAND CONTROL READY", 6:"HAND CONTROL MODE", 8:"EXIT MODE", 11:"ACS CONTROL MODE"}
        self.pod_status = {
            "seq":-1, "PodState":-1, "Velocity": -1 , "SteeringPercent" : 0, "StateOfCharge" : 0,
            "VSupply12" : 0, "VBat48": 0, "VmsFaultCode": 0, "McuFaultCode": 0, "ParkBrakeState": 0,
            "EStop": 0, "RfEstopTriggered": 0, "LeftDoorClosed": 0, "RightDoorClosed": 0,
            "FrontLeftOuterUltrasonic": 255, "FrontLeftCentreUltrasonic": 255, "FrontRightCentreUltrasonic": 255, "FrontRightOuterUltrasonic": 255,
            "RearLeftOuterUltrasonic": 255, "RearLeftCentreUltrasonic": 255, "RearRightCentreUltrasonic": 255, "RearRightOuterUltrasonic": 255, "joystick_init" : 0,
            "isFrontSafeStop" : "SAFE", "isRearSafeStop" : "SAFE", "initalized" : False, "leftWHeelCount" : 0, "rightWheelCount" : 0, "trip_odom" : 0, "ReqSteering" : 0, "ReqSpeed" : 0,
            "global_manual_odom" : -1, "global_auto_odom" : -1, "ts" : -1, "isAuto" : 0, "McuTemp" : 0, "MotorTemp" : 0, 'estimated_acc' : 0, 'prev_velocity' : 0, 'velocityms' : 0
            }
        self.send_acs_to_pod_command()
        super(RdmPodToAcsPublisher, self).__init__(*args, **kwargs)


def on_shutdown():
    '''Tornado close handler on keyboard interupt'''
    print('Shutting down')
    tornado.ioloop.IOLoop.instance().stop()


if __name__ == '__main__':
    tornado.options.parse_command_line()
    APP_IOLOOP = tornado.ioloop.IOLoop.instance()  # tornado IOLOOP assign    
    signal.signal(signal.SIGINT,
                  lambda sig,
                  frame: APP_IOLOOP.add_callback_from_signal(on_shutdown))
    RdmPodToAcsPublisher()
    APP_IOLOOP.start()


