# This Python file uses the following encoding: utf-8
"""autogenerated by genpy from moovita_message_definitions/pod_to_acs.msg. Do not edit."""
import sys
python3 = True if sys.hexversion > 0x03000000 else False
import genpy
import struct

import std_msgs.msg

class pod_to_acs(genpy.Message):
  _md5sum = "891a9c99084bde9b09aeea507c4ebb4e"
  _type = "moovita_message_definitions/pod_to_acs"
  _has_header = True #flag to mark the presence of a Header object
  _full_text = """## POD to ACS v1.4   #12/4/18 21:48

Header header

uint8 FrontLeftWheelCnt
uint8 FrontRightWheelCnt
uint8 RearLeftWheelCnt
uint8 RearRightWheelCnt

uint8 PodState
int16 Velocity
int8 SteeringPercent
uint8 StateOfCharge
int16 McuBatteryCurrent
int16 TotalBatteryCurrent
int16 VSupply12
int16 VBat48
int16 McuTemp
int16 MotorTemp
uint8 VmsFaultCode
uint8 McuFaultCode


uint8 ParkBrakeState
bool ParkBrakePressureAchieved
uint8 MotorBrakeState
bool HeadlampsAreOn
bool InteriorLightsAreOn
bool LeftDiIsActive
bool RightDiIsActive

bool EStopTriggered
bool SafetyLidarTriggered
bool RfEstopTriggered

bool LeftDoorButtonPressed
bool LeftDoorClosed
bool RightDoorButtonPressed
bool RightDoorClosed

uint8 FrontLeftOuterUltrasonic
uint8 FrontLeftCentreUltrasonic
uint8 FrontRightCentreUltrasonic
uint8 FrontRightOuterUltrasonic

uint8 RearLeftOuterUltrasonic
uint8 RearLeftCentreUltrasonic
uint8 RearRightCentreUltrasonic
uint8 RearRightOuterUltrasonic


================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id
"""
  __slots__ = ['header','FrontLeftWheelCnt','FrontRightWheelCnt','RearLeftWheelCnt','RearRightWheelCnt','PodState','Velocity','SteeringPercent','StateOfCharge','McuBatteryCurrent','TotalBatteryCurrent','VSupply12','VBat48','McuTemp','MotorTemp','VmsFaultCode','McuFaultCode','ParkBrakeState','ParkBrakePressureAchieved','MotorBrakeState','HeadlampsAreOn','InteriorLightsAreOn','LeftDiIsActive','RightDiIsActive','EStopTriggered','SafetyLidarTriggered','RfEstopTriggered','LeftDoorButtonPressed','LeftDoorClosed','RightDoorButtonPressed','RightDoorClosed','FrontLeftOuterUltrasonic','FrontLeftCentreUltrasonic','FrontRightCentreUltrasonic','FrontRightOuterUltrasonic','RearLeftOuterUltrasonic','RearLeftCentreUltrasonic','RearRightCentreUltrasonic','RearRightOuterUltrasonic']
  _slot_types = ['std_msgs/Header','uint8','uint8','uint8','uint8','uint8','int16','int8','uint8','int16','int16','int16','int16','int16','int16','uint8','uint8','uint8','bool','uint8','bool','bool','bool','bool','bool','bool','bool','bool','bool','bool','bool','uint8','uint8','uint8','uint8','uint8','uint8','uint8','uint8']

  def __init__(self, *args, **kwds):
    """
    Constructor. Any message fields that are implicitly/explicitly
    set to None will be assigned a default value. The recommend
    use is keyword arguments as this is more robust to future message
    changes.  You cannot mix in-order arguments and keyword arguments.

    The available fields are:
       header,FrontLeftWheelCnt,FrontRightWheelCnt,RearLeftWheelCnt,RearRightWheelCnt,PodState,Velocity,SteeringPercent,StateOfCharge,McuBatteryCurrent,TotalBatteryCurrent,VSupply12,VBat48,McuTemp,MotorTemp,VmsFaultCode,McuFaultCode,ParkBrakeState,ParkBrakePressureAchieved,MotorBrakeState,HeadlampsAreOn,InteriorLightsAreOn,LeftDiIsActive,RightDiIsActive,EStopTriggered,SafetyLidarTriggered,RfEstopTriggered,LeftDoorButtonPressed,LeftDoorClosed,RightDoorButtonPressed,RightDoorClosed,FrontLeftOuterUltrasonic,FrontLeftCentreUltrasonic,FrontRightCentreUltrasonic,FrontRightOuterUltrasonic,RearLeftOuterUltrasonic,RearLeftCentreUltrasonic,RearRightCentreUltrasonic,RearRightOuterUltrasonic

    :param args: complete set of field values, in .msg order
    :param kwds: use keyword arguments corresponding to message field names
    to set specific fields.
    """
    if args or kwds:
      super(pod_to_acs, self).__init__(*args, **kwds)
      #message fields cannot be None, assign default values for those that are
      if self.header is None:
        self.header = std_msgs.msg.Header()
      if self.FrontLeftWheelCnt is None:
        self.FrontLeftWheelCnt = 0
      if self.FrontRightWheelCnt is None:
        self.FrontRightWheelCnt = 0
      if self.RearLeftWheelCnt is None:
        self.RearLeftWheelCnt = 0
      if self.RearRightWheelCnt is None:
        self.RearRightWheelCnt = 0
      if self.PodState is None:
        self.PodState = 0
      if self.Velocity is None:
        self.Velocity = 0
      if self.SteeringPercent is None:
        self.SteeringPercent = 0
      if self.StateOfCharge is None:
        self.StateOfCharge = 0
      if self.McuBatteryCurrent is None:
        self.McuBatteryCurrent = 0
      if self.TotalBatteryCurrent is None:
        self.TotalBatteryCurrent = 0
      if self.VSupply12 is None:
        self.VSupply12 = 0
      if self.VBat48 is None:
        self.VBat48 = 0
      if self.McuTemp is None:
        self.McuTemp = 0
      if self.MotorTemp is None:
        self.MotorTemp = 0
      if self.VmsFaultCode is None:
        self.VmsFaultCode = 0
      if self.McuFaultCode is None:
        self.McuFaultCode = 0
      if self.ParkBrakeState is None:
        self.ParkBrakeState = 0
      if self.ParkBrakePressureAchieved is None:
        self.ParkBrakePressureAchieved = False
      if self.MotorBrakeState is None:
        self.MotorBrakeState = 0
      if self.HeadlampsAreOn is None:
        self.HeadlampsAreOn = False
      if self.InteriorLightsAreOn is None:
        self.InteriorLightsAreOn = False
      if self.LeftDiIsActive is None:
        self.LeftDiIsActive = False
      if self.RightDiIsActive is None:
        self.RightDiIsActive = False
      if self.EStopTriggered is None:
        self.EStopTriggered = False
      if self.SafetyLidarTriggered is None:
        self.SafetyLidarTriggered = False
      if self.RfEstopTriggered is None:
        self.RfEstopTriggered = False
      if self.LeftDoorButtonPressed is None:
        self.LeftDoorButtonPressed = False
      if self.LeftDoorClosed is None:
        self.LeftDoorClosed = False
      if self.RightDoorButtonPressed is None:
        self.RightDoorButtonPressed = False
      if self.RightDoorClosed is None:
        self.RightDoorClosed = False
      if self.FrontLeftOuterUltrasonic is None:
        self.FrontLeftOuterUltrasonic = 0
      if self.FrontLeftCentreUltrasonic is None:
        self.FrontLeftCentreUltrasonic = 0
      if self.FrontRightCentreUltrasonic is None:
        self.FrontRightCentreUltrasonic = 0
      if self.FrontRightOuterUltrasonic is None:
        self.FrontRightOuterUltrasonic = 0
      if self.RearLeftOuterUltrasonic is None:
        self.RearLeftOuterUltrasonic = 0
      if self.RearLeftCentreUltrasonic is None:
        self.RearLeftCentreUltrasonic = 0
      if self.RearRightCentreUltrasonic is None:
        self.RearRightCentreUltrasonic = 0
      if self.RearRightOuterUltrasonic is None:
        self.RearRightOuterUltrasonic = 0
    else:
      self.header = std_msgs.msg.Header()
      self.FrontLeftWheelCnt = 0
      self.FrontRightWheelCnt = 0
      self.RearLeftWheelCnt = 0
      self.RearRightWheelCnt = 0
      self.PodState = 0
      self.Velocity = 0
      self.SteeringPercent = 0
      self.StateOfCharge = 0
      self.McuBatteryCurrent = 0
      self.TotalBatteryCurrent = 0
      self.VSupply12 = 0
      self.VBat48 = 0
      self.McuTemp = 0
      self.MotorTemp = 0
      self.VmsFaultCode = 0
      self.McuFaultCode = 0
      self.ParkBrakeState = 0
      self.ParkBrakePressureAchieved = False
      self.MotorBrakeState = 0
      self.HeadlampsAreOn = False
      self.InteriorLightsAreOn = False
      self.LeftDiIsActive = False
      self.RightDiIsActive = False
      self.EStopTriggered = False
      self.SafetyLidarTriggered = False
      self.RfEstopTriggered = False
      self.LeftDoorButtonPressed = False
      self.LeftDoorClosed = False
      self.RightDoorButtonPressed = False
      self.RightDoorClosed = False
      self.FrontLeftOuterUltrasonic = 0
      self.FrontLeftCentreUltrasonic = 0
      self.FrontRightCentreUltrasonic = 0
      self.FrontRightOuterUltrasonic = 0
      self.RearLeftOuterUltrasonic = 0
      self.RearLeftCentreUltrasonic = 0
      self.RearRightCentreUltrasonic = 0
      self.RearRightOuterUltrasonic = 0

  def _get_types(self):
    """
    internal API method
    """
    return self._slot_types

  def serialize(self, buff):
    """
    serialize message into buffer
    :param buff: buffer, ``StringIO``
    """
    try:
      _x = self
      buff.write(_struct_3I.pack(_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs))
      _x = self.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_struct_5BhbB6h24B.pack(_x.FrontLeftWheelCnt, _x.FrontRightWheelCnt, _x.RearLeftWheelCnt, _x.RearRightWheelCnt, _x.PodState, _x.Velocity, _x.SteeringPercent, _x.StateOfCharge, _x.McuBatteryCurrent, _x.TotalBatteryCurrent, _x.VSupply12, _x.VBat48, _x.McuTemp, _x.MotorTemp, _x.VmsFaultCode, _x.McuFaultCode, _x.ParkBrakeState, _x.ParkBrakePressureAchieved, _x.MotorBrakeState, _x.HeadlampsAreOn, _x.InteriorLightsAreOn, _x.LeftDiIsActive, _x.RightDiIsActive, _x.EStopTriggered, _x.SafetyLidarTriggered, _x.RfEstopTriggered, _x.LeftDoorButtonPressed, _x.LeftDoorClosed, _x.RightDoorButtonPressed, _x.RightDoorClosed, _x.FrontLeftOuterUltrasonic, _x.FrontLeftCentreUltrasonic, _x.FrontRightCentreUltrasonic, _x.FrontRightOuterUltrasonic, _x.RearLeftOuterUltrasonic, _x.RearLeftCentreUltrasonic, _x.RearRightCentreUltrasonic, _x.RearRightOuterUltrasonic))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize(self, str):
    """
    unpack serialized message in str into this message instance
    :param str: byte array of serialized message, ``str``
    """
    try:
      if self.header is None:
        self.header = std_msgs.msg.Header()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs,) = _struct_3I.unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.header.frame_id = str[start:end].decode('utf-8')
      else:
        self.header.frame_id = str[start:end]
      _x = self
      start = end
      end += 45
      (_x.FrontLeftWheelCnt, _x.FrontRightWheelCnt, _x.RearLeftWheelCnt, _x.RearRightWheelCnt, _x.PodState, _x.Velocity, _x.SteeringPercent, _x.StateOfCharge, _x.McuBatteryCurrent, _x.TotalBatteryCurrent, _x.VSupply12, _x.VBat48, _x.McuTemp, _x.MotorTemp, _x.VmsFaultCode, _x.McuFaultCode, _x.ParkBrakeState, _x.ParkBrakePressureAchieved, _x.MotorBrakeState, _x.HeadlampsAreOn, _x.InteriorLightsAreOn, _x.LeftDiIsActive, _x.RightDiIsActive, _x.EStopTriggered, _x.SafetyLidarTriggered, _x.RfEstopTriggered, _x.LeftDoorButtonPressed, _x.LeftDoorClosed, _x.RightDoorButtonPressed, _x.RightDoorClosed, _x.FrontLeftOuterUltrasonic, _x.FrontLeftCentreUltrasonic, _x.FrontRightCentreUltrasonic, _x.FrontRightOuterUltrasonic, _x.RearLeftOuterUltrasonic, _x.RearLeftCentreUltrasonic, _x.RearRightCentreUltrasonic, _x.RearRightOuterUltrasonic,) = _struct_5BhbB6h24B.unpack(str[start:end])
      self.ParkBrakePressureAchieved = bool(self.ParkBrakePressureAchieved)
      self.HeadlampsAreOn = bool(self.HeadlampsAreOn)
      self.InteriorLightsAreOn = bool(self.InteriorLightsAreOn)
      self.LeftDiIsActive = bool(self.LeftDiIsActive)
      self.RightDiIsActive = bool(self.RightDiIsActive)
      self.EStopTriggered = bool(self.EStopTriggered)
      self.SafetyLidarTriggered = bool(self.SafetyLidarTriggered)
      self.RfEstopTriggered = bool(self.RfEstopTriggered)
      self.LeftDoorButtonPressed = bool(self.LeftDoorButtonPressed)
      self.LeftDoorClosed = bool(self.LeftDoorClosed)
      self.RightDoorButtonPressed = bool(self.RightDoorButtonPressed)
      self.RightDoorClosed = bool(self.RightDoorClosed)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill


  def serialize_numpy(self, buff, numpy):
    """
    serialize message with numpy array types into buffer
    :param buff: buffer, ``StringIO``
    :param numpy: numpy python module
    """
    try:
      _x = self
      buff.write(_struct_3I.pack(_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs))
      _x = self.header.frame_id
      length = len(_x)
      if python3 or type(_x) == unicode:
        _x = _x.encode('utf-8')
        length = len(_x)
      if python3:
        buff.write(struct.pack('<I%sB'%length, length, *_x))
      else:
        buff.write(struct.pack('<I%ss'%length, length, _x))
      _x = self
      buff.write(_struct_5BhbB6h24B.pack(_x.FrontLeftWheelCnt, _x.FrontRightWheelCnt, _x.RearLeftWheelCnt, _x.RearRightWheelCnt, _x.PodState, _x.Velocity, _x.SteeringPercent, _x.StateOfCharge, _x.McuBatteryCurrent, _x.TotalBatteryCurrent, _x.VSupply12, _x.VBat48, _x.McuTemp, _x.MotorTemp, _x.VmsFaultCode, _x.McuFaultCode, _x.ParkBrakeState, _x.ParkBrakePressureAchieved, _x.MotorBrakeState, _x.HeadlampsAreOn, _x.InteriorLightsAreOn, _x.LeftDiIsActive, _x.RightDiIsActive, _x.EStopTriggered, _x.SafetyLidarTriggered, _x.RfEstopTriggered, _x.LeftDoorButtonPressed, _x.LeftDoorClosed, _x.RightDoorButtonPressed, _x.RightDoorClosed, _x.FrontLeftOuterUltrasonic, _x.FrontLeftCentreUltrasonic, _x.FrontRightCentreUltrasonic, _x.FrontRightOuterUltrasonic, _x.RearLeftOuterUltrasonic, _x.RearLeftCentreUltrasonic, _x.RearRightCentreUltrasonic, _x.RearRightOuterUltrasonic))
    except struct.error as se: self._check_types(struct.error("%s: '%s' when writing '%s'" % (type(se), str(se), str(locals().get('_x', self)))))
    except TypeError as te: self._check_types(ValueError("%s: '%s' when writing '%s'" % (type(te), str(te), str(locals().get('_x', self)))))

  def deserialize_numpy(self, str, numpy):
    """
    unpack serialized message in str into this message instance using numpy for array types
    :param str: byte array of serialized message, ``str``
    :param numpy: numpy python module
    """
    try:
      if self.header is None:
        self.header = std_msgs.msg.Header()
      end = 0
      _x = self
      start = end
      end += 12
      (_x.header.seq, _x.header.stamp.secs, _x.header.stamp.nsecs,) = _struct_3I.unpack(str[start:end])
      start = end
      end += 4
      (length,) = _struct_I.unpack(str[start:end])
      start = end
      end += length
      if python3:
        self.header.frame_id = str[start:end].decode('utf-8')
      else:
        self.header.frame_id = str[start:end]
      _x = self
      start = end
      end += 45
      (_x.FrontLeftWheelCnt, _x.FrontRightWheelCnt, _x.RearLeftWheelCnt, _x.RearRightWheelCnt, _x.PodState, _x.Velocity, _x.SteeringPercent, _x.StateOfCharge, _x.McuBatteryCurrent, _x.TotalBatteryCurrent, _x.VSupply12, _x.VBat48, _x.McuTemp, _x.MotorTemp, _x.VmsFaultCode, _x.McuFaultCode, _x.ParkBrakeState, _x.ParkBrakePressureAchieved, _x.MotorBrakeState, _x.HeadlampsAreOn, _x.InteriorLightsAreOn, _x.LeftDiIsActive, _x.RightDiIsActive, _x.EStopTriggered, _x.SafetyLidarTriggered, _x.RfEstopTriggered, _x.LeftDoorButtonPressed, _x.LeftDoorClosed, _x.RightDoorButtonPressed, _x.RightDoorClosed, _x.FrontLeftOuterUltrasonic, _x.FrontLeftCentreUltrasonic, _x.FrontRightCentreUltrasonic, _x.FrontRightOuterUltrasonic, _x.RearLeftOuterUltrasonic, _x.RearLeftCentreUltrasonic, _x.RearRightCentreUltrasonic, _x.RearRightOuterUltrasonic,) = _struct_5BhbB6h24B.unpack(str[start:end])
      self.ParkBrakePressureAchieved = bool(self.ParkBrakePressureAchieved)
      self.HeadlampsAreOn = bool(self.HeadlampsAreOn)
      self.InteriorLightsAreOn = bool(self.InteriorLightsAreOn)
      self.LeftDiIsActive = bool(self.LeftDiIsActive)
      self.RightDiIsActive = bool(self.RightDiIsActive)
      self.EStopTriggered = bool(self.EStopTriggered)
      self.SafetyLidarTriggered = bool(self.SafetyLidarTriggered)
      self.RfEstopTriggered = bool(self.RfEstopTriggered)
      self.LeftDoorButtonPressed = bool(self.LeftDoorButtonPressed)
      self.LeftDoorClosed = bool(self.LeftDoorClosed)
      self.RightDoorButtonPressed = bool(self.RightDoorButtonPressed)
      self.RightDoorClosed = bool(self.RightDoorClosed)
      return self
    except struct.error as e:
      raise genpy.DeserializationError(e) #most likely buffer underfill

_struct_I = genpy.struct_I
_struct_3I = struct.Struct("<3I")
_struct_5BhbB6h24B = struct.Struct("<5BhbB6h24B")
