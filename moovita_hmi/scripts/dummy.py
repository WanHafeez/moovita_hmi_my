import rospy
from std_msgs.msg import String
from geometry_msgs.msg import TwistStamped,Pose,Twist, Vector3
from sensor_msgs.msg import PointCloud2, PointField
import redis, math, time, os
from moovita_message_definitions.msg import wheel_console
from moovita_message_definitions.msg import pod_to_acs
from moovita_message_definitions.msg import acs_to_pod
from moovita_message_definitions.msg import rdm_buttons
from moovita_message_definitions.msg import auto_controller

# from datetime import timedelta
import std_msgs.msg
# from ConfigParser import SafeConfigParser
# import tornado.web
# import tornado.websocket
# import tornado.options
# import tornado.ioloop
# from tornado import gen
# import signal
# import json

def talker():
    # pub = rospy.Publisher('chatter', String, queue_size=10)
    # rospy.Subscriber("/pod_to_acs", pod_to_acs, self.pod_callback)
    
    pub = rospy.Publisher('auto_controller', auto_controller, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(100) # 10hz

    while not rospy.is_shutdown():
        # hello_str = "hello world %s" % rospy.get_time()
        data = auto_controller()
        data.header.stamp = rospy.Time.now()
        data.header.frame_id = "moovita"
        data.cmd_steering = 10
        data.cmd_speed = 10
        data.is_brake = True
        data.is_estop = True
        rospy.loginfo(data)
        pub.publish(data)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass