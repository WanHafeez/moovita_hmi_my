import csv
#import av_sw_UTMConvert as UTMConvert
#import utm
import json

DEBUG = True
MAP = 'ONE_NORTH'

with open ('/home/fukeong/Desktop/av/dds/moovita_av/modules/missionPlanner/config/rndf/singapore/ngee_ann_rdm_full(26.11.18)/curb.csv','rb') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=';')
    shapes = {}
    i = 0
    for row in csvreader:
        if i == 0:
            i += 1
            continue

        print row
        if int(row[0]) in shapes:
            shapes[int(row[0])].append((float(row[1]), float(row[2])))
        else:
            shapes[int(row[0])] = [[float(row[1]), float(row[2])]]
        
        i += 1
        
    _output =  "var outline = " + json.dumps(shapes)
    if DEBUG:
        print _output
    
    f = open( 'ngee_ann_full-13.11.2018/outline.js', 'w' )
    f.write( 'var outline = ' + (json.dumps(shapes)) + ';\n' )
    f.close()
