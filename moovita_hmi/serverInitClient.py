#!/usr/bin/env python
# -*- coding: utf-8 -*-

#==============================================================================
#title           :serverInitClient.py
#description     :This file initialize daemon server Moovita Health Monitoring System.
#author          :[Chia Fu Keong <fukeong@moovita.com>]
#copyright       :Copyright 2016, Moovita
#date            :20171123
#version         :0.1
#status          :Development
#usage           :python serverInitClient.py [-s Server Id: eg 0 | 1 | 2]
#version_notes   :First version
#python_version  :2.7.x
#==============================================================================

'''
 * Copyright (C) Moovita - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Chia Fukeong <fukeong@moovita.com>, November 2017
 *
'''
#**********************************************************************/
#***   Includes                                                     ***/
#**********************************************************************/
import argparse
import atexit
import const
import datetime
import logging
import os, os.path
import psutil
import redis
import shlex, subprocess
import signal
import sys
import time


#**********************************************************************/
#***   Global Variables                                             ***/
#**********************************************************************/
CWD = os.getcwd()
CHECK_INTERNAL = 2
FLAGS = None
IS_STOP = False
LOG_FILENAME = 'logs/'+str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'.log'
REDIS_PUB_CHANNEL = "mooHmClient"
REDIS_SUB_CHANNEL = "mooHmServer"
REDIS_CON = ''
REDIS_HOST = '127.0.0.1'
REDIS_THREAD = ''
SERVER_ID = 0


#**********************************************************************/
#***   Prototypes                                                   ***/
#**********************************************************************/
def check_alive(name):
    for proc in psutil.process_iter():
        # Check whether the process name matches
        if proc.name() == name:
            return 1
    return 0


def goodbye(sId):
    print const.PCOLOR_C.WARNING + "[INFO][EXITING] " + const.PCOLOR_C.ENDC + " Killing program in server " + str(sId)
    stop_all(sId)


def main():
    ''' Declaration '''
    global FLAGS
    
    ''' Run all modules in particular server '''
    try:
        sId = int(FLAGS.s)
        if sId < len(const.SERVER_MODULES):
        
            for i in range(0, len(const.SERVER_MODULES_DIR[sId])):
                ''' Start modules '''
                start_process(const.SERVER_MODULES[sId][i], const.SERVER_MODULES_DIR[sId][i])
            
        else:
            print const.PCOLOR_C.WARNING + "[ERROR][#01] " + const.PCOLOR_C.ENDC + " INVALID SERVER ID."
    
    except ValueError:
        print const.PCOLOR_C.WARNING + "[ERROR][#02] " + const.PCOLOR_C.ENDC + " INVALID SERVER ID."
    

    #stop_all(sId) # REMOVE THIS AFTER TEST
    
    ''' Loop and check modules alive status '''
    _modules_states = [0] * len(const.SERVER_MODULES_DIR[sId])
    while not IS_STOP:
        for i in range(0, len(const.SERVER_MODULES_DIR[sId])):
            ''' Start modules '''
            _modules_states[i] = check_alive(const.SERVER_MODULES[sId][i])
            if _modules_states[i] == 0:
                start_process(const.SERVER_MODULES[sId][i], const.SERVER_MODULES_DIR[sId][i])
                time.sleep(0.5)
        
        REDIS_CON.publish(REDIS_PUB_CHANNEL, str(sId) + "[BRK]" + ",".join(map( str, _modules_states)))
        print _modules_states
        time.sleep(CHECK_INTERNAL)
            

    ''' Stop all programs '''
    stop_all(sId)
    
    # TODO: RUN start script
    # Then set socket, 
    # Run while loop check program died and report.

def on_redis_message(msg):
    print const.PCOLOR_C.OKGREEN + '[INFO] Redis message: ' + const.PCOLOR_C.ENDC + msg['data']
    msgList = msg['data'].split(',')
    
    if msgList[0] == 'START':
        start_process(const.SERVER_MODULES[int(msgList[1])][int(msgList[2])], 
        const.SERVER_MODULES_DIR[int(msgList[1])][int(msgList[2])])
    elif msgList[0] == 'STOP':
        stop_process(const.SERVER_MODULES[int(msgList[1])][int(msgList[2])])
    

def redis_start():
    ''' Start Redis server and return connection id '''
    _dbcon = redis.StrictRedis(host=REDIS_HOST, port=6379, db=0)
    try:
        _dbcon.ping()
        print const.PCOLOR_C.OKGREEN + "[INFO]" + const.PCOLOR_C.ENDC + "Redis Running"
    except:
        time.sleep(1)
        try:
            print _dbcon.ping()
            print const.PCOLOR_C.OKGREEN + "[INFO]" + const.PCOLOR_C.ENDC +  "Redis Started"
        except:
            print const.PCOLOR_C.WARNING + "[ERROR][#02] " + const.PCOLOR_C.ENDC + "Redis Can't start"
            sys.exit(0)
    return _dbcon


def signal_handler(signal, frame):
    IS_STOP = True
    print const.PCOLOR_C.WARNING + "[INFO]" + const.PCOLOR_C.ENDC + "Cltr + c Exit"
    REDIS_THREAD.stop()
    goodbye(SERVER_ID)
    time.sleep(1.01)
    sys.exit(0)


def stop_all(serverId):
    for proc in psutil.process_iter():
        # check whether the process name matches
        if proc.name() in const.SERVER_MODULES[serverId]:
            proc.kill()
            print const.PCOLOR_C.WARNING + "[INFO]" + const.PCOLOR_C.ENDC + " Killed " + proc.name()


def stop_process(name):
    for proc in psutil.process_iter():
        # check whether the process name matches
        # print proc
        if proc.name() == name:
            proc.kill()
            print const.PCOLOR_C.WARNING + "[INFO]" + const.PCOLOR_C.ENDC + " Killed " + proc.name()


def start_process(name, path):
    """
    Starts a process in the background and writes a PID file
    returns integer: pid
    """

    # Check if the process is already running
    #status, pid = processStatus(name)

    #if status == RUNNING:
        #raise AlreadyStartedError(pid)

    if name == 'mooEyeObjDetector':
        cmd = name + " detector demo ../cfg/coco.data ../cfg/yolo.cfg ../backup/yolo.weights"
    elif name == 'mooPclObjDetectorComponent':
        cmd = name + " input:=/rfans_driver/rfans_points"
    elif name == 'mooPclObjRecognitor':
        cmd = name + " -c -1"
    elif name == const.MODULE_PARKING:
        cmd = name + " ttt 0 ../util/Park_Det_User_Config.yml"
    else:
        cmd = name

    # Start process

    process = subprocess.Popen("nohup " + path + cmd + " > "+ CWD + "/logs/" + name + ".log &", cwd=path, shell=True)
    print "OK"
    print process.pid
    
    print const.PCOLOR_C.OKGREEN + '[INFO] ' + const.PCOLOR_C.ENDC + 'Started ' + name + 'on' + process.pid
    


#**********************************************************************/
#***   Main                                                         ***/
#**********************************************************************/
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', type=str, default="", help = 'Server id')
    FLAGS, unparsed = parser.parse_known_args()
    
    ''' Exception '''
    if not FLAGS.s or not FLAGS.s.isdigit():
        print "python serverInitClient.py [-s serverId]"
        sys.exit(1)
    
    SERVER_ID = int(FLAGS.s)
    
    ''' Register exit event '''
    atexit.register(goodbye, int(FLAGS.s))
    
    ''' Start redis '''
    REDIS_CON = redis_start() 
    logging.basicConfig(filename=LOG_FILENAME, level=logging.DEBUG, format='%(asctime)s %(message)s')
    
    ''' Add signal control '''
    for sig in (signal.SIGABRT, signal.SIGILL, signal.SIGINT, signal.SIGSEGV, signal.SIGTERM):
        signal.signal(sig, signal_handler)
    
    ''' Run subscription in thread '''
    p = REDIS_CON.pubsub()
    p.subscribe(**{REDIS_SUB_CHANNEL: on_redis_message})
    REDIS_THREAD = p.run_in_thread(sleep_time=0.001)
    
    ''' Proceed main control'''
    main()
