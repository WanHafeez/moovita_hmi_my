#!/usr/bin/env python
import math, time, calendar, os, sys
filepath = os.path.dirname(os.path.realpath(__file__))
sys.path.append(filepath + "/");
import rticonnextdds_connector as rti
import redis, math
import pprint

from ConfigParser import SafeConfigParser
import tornadoredis

import tornado.web
import tornado.websocket
import tornado.options
import tornado.ioloop
from tornado import gen
import logging
from datetime import timedelta
import signal



def redis_start():
    """Start Redis server and return connection id"""
    _dbcon = redis.StrictRedis(host='localhost', port=6379, db=0)
    try:
        _dbcon.ping()
        print "Redis Running"
    except:
        print "Redis Starting"
        os.system('redis-server > redis_log.txt &')
        time.sleep(1)
        try:
            print _dbcon.ping()
            print "Redis Started"
        except:
            print "Redis Can't start'"
            sys.exit(0)
    return _dbcon



def current_milli_time():
    '''Get current timestamp in millisecond'''
    return int(round(time.time() * 1000))


REDIS_CON = redis_start()




class DDSROSHandler(object):  # noqa pylint: disable=R0902
    '''AutoHandler . Client, server communication handeled here.'''


    def _vector2(self, x,y):
        return dict({'x':x, 'y':y})

    @tornado.gen.engine
    def test_loop(self):
        
        while True:
            _last_time = current_milli_time()

            self.Reqpath['stopPos'] = []
            self.Reqpath['startPos']['x'] = 1
            self.Reqpath['startPos']['y'] = 1

            self.Reqpath['stopPos'].append(self._vector2(_last_time, _last_time))
            
            self.Reqpath['stopPos'].append(self._vector2(2,2))
            
            self.ReqGlobalPathOutput.instance.setDictionary(self.Reqpath);
            self.ReqGlobalPathOutput.write()


            print self.Reqpath
            
            yield gen.sleep(0.1)

    def __init__(self, *args, **kwargs):
        """Initalize self variables"""

        self.Reqpath = {'startPos':{'x':0, 'y':0}, 'stopPos':[]}
        self.connector = rti.Connector("DDSROS_ParticipantLibrary::DDSROS",filepath + "/topicGlobalPathRequest.xml");
        self.ReqGlobalPathOutput  = self.connector.getOutput("RequestGlobalPathPublisher::RequestGlobalPathWriter")
        self.test_loop()
        
        super(DDSROSHandler, self).__init__(*args, **kwargs)
    

if __name__ == "__main__":
    tornado.options.parse_command_line()
    APP_IOLOOP = tornado.ioloop.IOLoop.instance()  # tornado IOLOOP assign    
    signal.signal(signal.SIGINT,
                  lambda sig,
                  frame: APP_IOLOOP.add_callback_from_signal(on_shutdown))
    DDSROSHandler()
    APP_IOLOOP.start()


